#pragma once

#include "uni_measurement_manager/measurement_device_impl.h"

#include <map>

namespace unim = Uni_measurement_manager;

namespace Boxcar_application {

    class Boxcar : public unim::Measurement_device_impl {
        public:
            class Boxcar_drive : public unim::Measurement_device_impl::Drive_impl {
                public:
                    Boxcar_drive(Boxcar& boxcar, int id) :unim::Measurement_device_impl::Drive_impl(id), boxcar_(boxcar) {}
                    double min_position() { return 0.0; }
                    double max_position() { return 10.0; }
                    void move(double position);
                private:
                    Boxcar& boxcar_;
            };
            friend class Boxcar_drive;
            Boxcar();
            void connect();
            double read_out();
        private:
            std::map<double, double> map_;
            double max_error_;
        private:
            double position_;
    };

}
