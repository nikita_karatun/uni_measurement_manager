#include "boxcar.h"

#include <iostream>
#include <sstream>

#include <wx/log.h>

#include "boxcar_application.h"

namespace Boxcar_application {

    const char* test_data();

    double next_double(double from, double to) {
        int multiplier = 1;
        double diff = to - from;
        while (diff < 10000) {
            multiplier *= 10;
            diff *= 10;
        }
        int r = rand() % (int) diff;
        return from + (double) r / multiplier;
    }

    Boxcar::Boxcar() {
        double first_key = 1;
        double increment = 0.1;

        std::stringstream ss(test_data());
        std::string line;
        for (int line_index = 0; std::getline(ss, line, '\n'); ++line_index) {
            std::stringstream line_ss(line);
            std::string entry;
            for (int entry_index = 0; std::getline(line_ss, entry, '\t') && entry_index < 2; ++entry_index) {
                double number = atof(entry.c_str());
                if (entry_index == 1)
                    map_[first_key + line_index * increment] = number;
            }
        }

        // for (std::map<double, double>::iterator it = map_.begin(); it != map_.end(); ++it)
        //     std::cout << it->first << " - " << it->second << std::endl;

        srand(45);
        max_error_ = next_double(0, 0.1);

        add_drive(new Boxcar_drive(*this, 1));
    }

    void Boxcar::connect() {
        // if (Boxcar_application::device_disconnected_) todo restore
        //     throw unim::Measurement_device_exception("cannot connect!");
        wxLogMessage("connecting");
    }

    double Boxcar::read_out() {
        // if (Boxcar_application::device_disconnected_)
        //     throw unim::Measurement_device_exception("cannot connect!");
        std::map<double, double>::iterator it = map_.lower_bound(position_);
        double value = 0;

        if (it != map_.end()) {
            if (it->first == position_ || it == map_.begin())
                value = it->second;
            else {
                double second = it->second;
                --it;
                value = (second + it->second) / 2;
            }
        }
        double error = next_double(-1 * max_error_, max_error_);
        return value + value * error; // todo fix all 0 case strange plot behaviour
    }

    void Boxcar::Boxcar_drive::move(double position) {
        // if (Boxcar_application::device_disconnected_)
        //     throw unim::Measurement_device_exception("cannot connect!");
        boxcar_.position_ = position;
    }

    const char* test_data() {
        return "190	1.53213251892764E-7	2.80820474483056E-8	184.216890399441\n" \
            "192	2.11326146696943E-7	6.72712688382723E-8	124.280903751839\n" \
            "194	3.82740817801173E-7	1.42472907755982E-7	107.29312312553\n" \
            "196	5.73350566601466E-7	2.21219401525072E-7	103.399258237938\n" \
            "198	7.46361668734103E-7	3.12126326366603E-7	94.6547315391039\n" \
            "200	8.96580283943137E-7	3.98883777628968E-7	87.9357025524209\n" \
            "202	1.10642218434014E-6	5.14994835464935E-7	83.0295003003737\n" \
            "204	1.39842616765545E-6	6.8473707383037E-7	77.5289248497499\n" \
            "206	1.74149167404855E-6	8.56806696240302E-7	77.009637617207\n" \
            "208	2.24008041491955E-6	1.16489336529143E-6	70.994359322495\n" \
            "210	2.91647131629569E-6	1.55090328097675E-6	68.5682486825327\n" \
            "212	3.97584649840839E-6	2.13324932671015E-6	67.5969978357888\n" \
            "214	5.33914863655858E-6	2.88096333583057E-6	66.9835700053814\n" \
            "216	7.08648800499217E-6	3.83624081018371E-6	66.6312988178224\n" \
            "218	9.06810152711815E-6	4.91776703932397E-6	66.4371055120147\n" \
            "220	1.09814761470099E-5	5.9212095427353E-6	67.0625730472649\n" \
            "222	1.27692587181041E-5	6.89746256212517E-6	66.8690835991999\n" \
            "224	1.45940000251711E-5	7.84099249236384E-6	67.450826159977\n" \
            "226	1.6417404652082E-5	8.84982474172137E-6	67.0924581264866\n" \
            "228	1.79810923370259E-5	9.78330376984489E-6	66.0826331851686\n" \
            "230	1.93788423444809E-5	1.06525512896954E-5	64.968548911423\n" \
            "232	2.04359869158426E-5	1.14369199864085E-5	63.0216332980613\n" \
            "234	2.10493078077089E-5	1.20766339579827E-5	60.3229789659711\n" \
            "236	2.13006793031469E-5	1.24910257581111E-5	57.9488374698978\n" \
            "238	2.17499151769399E-5	1.30908841939569E-5	55.1221466511384\n" \
            "240	2.18245342657837E-5	1.35611679257431E-5	51.6619727151943\n" \
            "242	2.16698491278663E-5	1.38096990465072E-5	48.9179183741703\n" \
            "244	2.11770183482578E-5	1.39462504652977E-5	45.3518380804854\n" \
            "246	2.03448138103938E-5	1.37534595276822E-5	42.5104414717155\n" \
            "248	1.93776358049106E-5	1.36147861547957E-5	38.3224912285086\n" \
            "250	1.89342659358796E-5	1.37360262203641E-5	34.8468403542037\n" \
            "252	1.86165815928726E-5	1.38444363687131E-5	32.1561646380614\n" \
            "254	1.86157419669373E-5	1.42217183725944E-5	29.2320690853306\n" \
            "256	1.86364017285461E-5	1.46386776613692E-5	26.2150539264099\n" \
            "258	1.86472050211832E-5	1.50612811472304E-5	23.1879575701807\n" \
            "260	1.88388484851091E-5	1.56495217374923E-5	20.1383208900881";
    }
}

