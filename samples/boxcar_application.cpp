#include "boxcar_application.h"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "boxcar.h"

namespace Boxcar_application {

    std::auto_ptr<unim::Measurement_device> Device_factory::create_measurement_device() {
        return std::auto_ptr<unim::Measurement_device>(new Boxcar);
    }

    std::auto_ptr<unim::Hot_keys_handler> Context_factory::create_hot_keys_handler(unim::Measurement_context* measurement_context) {
        std::auto_ptr<unim::Hot_keys_handler> handler =
            unim::Measurement_context_factory_impl::create_hot_keys_handler(measurement_context);
        struct Switch_boxcar_connection_state: public unim::Hot_key_handler {
            void handle(unim::Measurement_context* measurement_context) {
                // Boxcar_application::device_disconnected_.store(!Boxcar_application::device_disconnected_.load());
                // std::cout << "device disconnected: " << Boxcar_application::device_disconnected_ << std::endl;
            }
        };
        handler->add_hot_key_handler(unim::Hot_key(true, true, wxKeyCode('D')), new Switch_boxcar_connection_state);
        return handler;
    }

    std::auto_ptr<unim::Properties_provider> Context_factory::create_properties_provider() {
        std::auto_ptr<unim::Properties_provider_impl> properties_provider(
                new unim::Properties_provider_impl);
        properties_provider
            ->add_property("application.title", "Boxcar")
            ->add_property("panels.positioning.title", "Delay")
            ->add_property("output_file.header.x", "Delay (V)")
            ->add_property("output_file.header.y", "Value (V)"); // todo move add property method to interface?
        return std::auto_ptr<unim::Properties_provider>(properties_provider.release());
    }

    Boxcar_application::Boxcar_application(): log_stream_("boxcar.log", std::ofstream::out) {}

    bool Boxcar_application::OnInit() {
        wxLog* logger = new wxLogStream(&log_stream_);
        wxLog::SetActiveTarget(logger);
        return unim::Application_impl::OnInit();
    }

    std::auto_ptr<unim::Measurement_context_factory> Boxcar_application::create_measurement_context_factory() {
        return std::auto_ptr<unim::Measurement_context_factory>(new Context_factory);
    }

    std::auto_ptr<unim::Measurement_device_factory> Boxcar_application::create_measurement_device_factory() {
        return std::auto_ptr<unim::Measurement_device_factory>(new Device_factory);
    }

    // std::atomic<bool> Boxcar_application::device_disconnected_; // todo restore
}

wxIMPLEMENT_APP(Boxcar_application::Boxcar_application);
