#pragma once

#include <string>

namespace Uni_measurement_manager {

    class Properties_provider {
        public:
            virtual ~Properties_provider() {}
            virtual std::string get_string(const std::string& key) = 0;
            virtual int get_int(const std::string& key) = 0;
            virtual double get_double(const std::string& key) = 0;
    };

}
