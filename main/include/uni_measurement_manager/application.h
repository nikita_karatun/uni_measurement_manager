#pragma once

#include <memory>

#include "wx/app.h"

namespace Uni_measurement_manager {

    class Measurement_context_factory;
    class Measurement_device_factory;

    class Application : public wxApp {
        public:
            virtual ~Application() {}
            virtual std::auto_ptr<Measurement_device_factory> create_measurement_device_factory() = 0;
        protected:
            virtual std::auto_ptr<Measurement_context_factory> create_measurement_context_factory() = 0;
    };

}
