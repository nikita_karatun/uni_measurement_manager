#pragma once 

#include <wx/frame.h>
#include <wx/menu.h>

#include "uni_measurement_manager/common.h"

namespace Uni_measurement_manager {

    class Main_frame : public wxFrame { // todo add context pointer?
        public:
            Main_frame(const wxString title) :wxFrame(NULL, wxID_ANY, title) {}
            virtual ~Main_frame() {}
            virtual void post_construct() {}
    };

    class Menu_bar : public wxMenuBar {
        public:
            virtual ~Menu_bar() {}
            virtual void enable_save_item(bool enable) = 0;
    };


    class Input_panel : public Abstract_panel {
        public:
            Input_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Input_panel() {}
    };

    class Measurement_device_info_payload;

    class Settings_panel : public Abstract_panel {
        public:
            Settings_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Settings_panel() {}
            virtual void on_device_info_message(const Measurement_device_info_payload&) = 0;
            virtual void write_settings() = 0;
    };

    class Buttons_panel : public Abstract_panel {
        public:
            Buttons_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Buttons_panel() {}
            virtual void on_start() = 0;
            virtual void on_pause() = 0;
            virtual void on_stop() = 0;
            virtual void on_pending() = 0;
    };

    class Measurement_payload;

    class Output_panel : public Abstract_panel {
        public:
            Output_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Output_panel() {}
            virtual void render_settings() = 0;
            virtual void clear() = 0;
            virtual void add_sample(const Measurement_payload&) = 0;
    };

    class Plot_panel : public Abstract_panel {
        public:
            struct Data_set {
                enum e {
                    MAIN = 0, CURRENT = 1, ALL
                };
            };
            Plot_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Plot_panel() {}
            virtual void set_default_axes() = 0;
            virtual void clear(Data_set::e data_set = Data_set::ALL) = 0;
            virtual void add_point(double x, double y, Data_set::e data_set = Data_set::ALL) = 0;
            virtual void set_x_limits(double begin, double end) = 0;
    };

}
