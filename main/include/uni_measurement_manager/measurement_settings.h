#pragma once 

#include <memory>

#include "uni_measurement_manager/iterator.h"

namespace Uni_measurement_manager {

    class Measurement_settings {
        public:
            class Drive {
                public:
                    typedef std::auto_ptr<Iterator<Drive*> > iterator_type;

                    virtual ~Drive() {}

                    virtual int id() = 0;
                    virtual double begin_position() = 0;
                    virtual double end_position() = 0;
                    virtual double step() = 0;

                    virtual void set_begin_position(double begin_value) = 0;
                    virtual void set_end_value(double end_value) = 0;
                    virtual void set_step(double step) = 0;

                    virtual void set_default_values() = 0;
            };


            virtual ~Measurement_settings() {}


            virtual int id() = 0;
            virtual double step() = 0;
            virtual int averaging_time() = 0;
            virtual int min_samples() = 0;
            virtual int max_samples() = 0;
            virtual double error() = 0;

            virtual void set_id(int id) = 0;
            virtual void set_step(double step) = 0;
            virtual void set_averaging_time(int averaging_time) = 0;
            virtual void set_min_samples(int min_samples) = 0;
            virtual void set_max_samples(int max_samples) = 0;
            virtual void set_error(double error) = 0;

            virtual Drive* get_or_create_drive_by_id(int id) = 0;
            virtual Drive* create_drive(int id) = 0;
            virtual Drive::iterator_type drives_iterator() = 0;

            virtual void set_default_values() = 0;

            virtual void load() = 0;
            virtual void save() = 0;
    };

}
