#pragma once

#include "uni_measurement_manager/hot_keys_handler.h"
#include "wx/event.h"
#include <map>

namespace Uni_measurement_manager {

    class Hot_keys_handler_impl : public Hot_keys_handler {
        public:
            Hot_keys_handler_impl(Measurement_context*);
            ~Hot_keys_handler_impl();
            void add_hot_key_handler(Hot_key, Hot_key_handler*);
            void connect(wxEvtHandler*);
            void connect_recursively(wxWindow*);
        private:
            void on_key_down(wxKeyEvent&);
            void on_focus_set(wxFocusEvent&);
            std::map<Hot_key, Hot_key_handler*> map_;
    };

}
