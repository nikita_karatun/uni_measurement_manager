#pragma once

class wxFrame;
class wxWindow;

namespace Uni_measurement_manager {

    class Main_frame;
    class Menu_bar;
    class Status_bar;
    class Plot_panel;
    class Input_panel;
    class Positioning_panel;
    class Settings_panel;
    class Output_panel;
    class Buttons_panel;

    struct Ui_actors {
        Main_frame* const main_frame_;
        Menu_bar* const menu_bar_;
        Status_bar* const status_bar_;
        Plot_panel* const plot_panel_;
        Input_panel* const input_panel_;
        Positioning_panel* const positioning_panel_;
        Settings_panel* const settings_panel_;
        Output_panel* const output_panel_;
        Buttons_panel* const buttons_panel_;
        Ui_actors(Main_frame* main_frame, Menu_bar* menu_bar, Status_bar* status_bar,
                Plot_panel* plot_panel, Input_panel* input_panel, Positioning_panel* positioning_panel,
                Settings_panel* settings_panel, Output_panel* output_panel, Buttons_panel* buttons_panel)
            :
                main_frame_(main_frame), menu_bar_(menu_bar), status_bar_(status_bar),
                plot_panel_(plot_panel), input_panel_(input_panel), positioning_panel_(positioning_panel),
                settings_panel_(settings_panel), output_panel_(output_panel), buttons_panel_(buttons_panel) 
        {}
    };

    class Ui_actors_factory {
        public:
            virtual ~Ui_actors_factory() {}
            virtual Ui_actors create_ui_actors() = 0;
            virtual Main_frame* create_main_frame() = 0;
            virtual Menu_bar* create_menu_bar() = 0;
            virtual Status_bar* create_status_bar(wxFrame* parent) = 0;
            virtual Plot_panel* create_plot_panel(wxWindow* parent) = 0;
            virtual Input_panel* create_input_panel(wxWindow* parent) = 0;
            virtual Positioning_panel* create_positioning_panel(wxWindow* parent) = 0;
            virtual Settings_panel* create_settings_panel(wxWindow* parent) = 0;
            virtual Output_panel* create_output_panel(wxWindow* parent) = 0;
            virtual Buttons_panel* create_buttons_panel(wxWindow* parent) = 0;
    };

}
