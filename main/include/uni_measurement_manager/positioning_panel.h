#pragma once

#include "uni_measurement_manager/common.h"
#include "uni_measurement_manager/enums.h"

#include "wx/window.h"

namespace Uni_measurement_manager {

    class Measurement_context;
    class Measurement_device_info_payload;
    class Drive_move_payload;

    class Positioning_panel : public Abstract_panel {
        public:
            Positioning_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :Abstract_panel(parent, measurement_context) 
            {}
            virtual ~Positioning_panel() {}
            virtual void switch_position() = 0;
            virtual void step_position(bool increment) = 0;
            virtual void on_device_info_message(const Measurement_device_info_payload&) = 0;
            virtual void on_drive_moved_message(const Drive_move_payload&) = 0;
            virtual void write_settings() = 0;
    };

}

