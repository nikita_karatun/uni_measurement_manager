#pragma once

#include "uni_measurement_manager/enums.h"

#include <string>

namespace Uni_measurement_manager {

    class Measurement_device_info_payload;
    class Measurement_payload;
    class Drive_move_payload;

    class Event_dispatcher {
        public:
            virtual ~Event_dispatcher() {}
            virtual void send_device_info_message(const Measurement_device_info_payload&) = 0;
            virtual void send_readout_message(const Measurement_payload&) = 0;
            virtual void send_drive_moved_message(const Drive_move_payload&) = 0;
            virtual void send_measurement_completed_message() = 0;
            virtual void send_paused_message() = 0;
            virtual void send_device_error_message(std::string message) = 0;
    };

}
