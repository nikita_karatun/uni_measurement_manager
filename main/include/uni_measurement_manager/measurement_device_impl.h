#pragma once // todo insert pragma once to all headers

#include "uni_measurement_manager/measurement_device.h"

#include <vector>
#include <set>
#include <sstream>

namespace Uni_measurement_manager {

    class Measurement_device_impl : public Measurement_device {
        public:
            class Drive_impl : public Measurement_device::Drive {
                public:
                    Drive_impl(int id);
                    int id();
                private:
                    const int id_;
            };
            ~Measurement_device_impl();
            void add_drive(Drive* drive);
            Drive::iterator_type drives_iterator();
        private:
            std::vector<Drive*> drives_;
            std::set<int> ids_;
    };

}
