#pragma once

#include "wx/event.h"

#include "uni_measurement_manager/measurement_context.h"

namespace Uni_measurement_manager {

    class Hot_key {
        public:
            Hot_key(bool ctrl_down, bool shift_down, wxKeyCode key_code)
                :ctrl_down_(ctrl_down), shift_down_(shift_down), key_code_(key_code)
            {}
            bool ctrl_down_;
            bool shift_down_;
            int key_code_;
            bool operator<(const Hot_key& o) const {
                if (ctrl_down_ != o.ctrl_down_) return ctrl_down_ < o.ctrl_down_;
                if (shift_down_ != o.shift_down_) return shift_down_ < o.shift_down_;
                return key_code_ < o.key_code_;
            }
    };

    class Hot_key_handler {
        public:
            virtual ~Hot_key_handler() {}
            virtual void handle(Measurement_context* measurement_context) = 0;
    };

    class Hot_keys_handler {
        public:
            Hot_keys_handler(Measurement_context* measurement_context)
                :measurement_context_(measurement_context)
            {}
            virtual ~Hot_keys_handler() {}
            virtual void add_hot_key_handler(Hot_key, Hot_key_handler*) = 0;
            virtual void connect(wxEvtHandler*) = 0;
            virtual void connect_recursively(wxWindow*) = 0;
        protected:
            Measurement_context* measurement_context_;
    };

}
