#pragma once

#include <map>

namespace Uni_measurement_manager {

    template<typename T>
        class Iterator {
            public:
                virtual ~Iterator() {}
                virtual bool has_next() = 0; // todo rename?
                virtual void next() = 0;
                virtual T get() = 0;
        };

    template<typename T, typename C>
        class Iterator_base_impl : public Iterator<T> {
            public:
                Iterator_base_impl(C& container) :current_(container.begin()), end_(container.end()) {}
                bool has_next() { return current_ != end_; }
                void next() { ++current_; }
            protected:
                typename C::iterator current_;
                typename C::iterator end_;
        };

    template<typename C>
        class Iterator_impl : public Iterator_base_impl<typename C::value_type, C> {
            public:
                typedef typename C::value_type value_type;
                Iterator_impl(C& container) :Iterator_base_impl<value_type, C>(container) {}
                value_type get() { return *Iterator_base_impl<value_type, C>::current_; }
        };

    template<typename T, typename K>
        class Iterator_impl<std::map<K, T> > : public Iterator_base_impl<T, std::map<K, T> > {
            public:
                typedef typename std::map<K, T> C;
                Iterator_impl(C& container) :Iterator_base_impl<T, C>(container) {}
                T get() { return Iterator_base_impl<T, C>::current_->second; }
        };

}
