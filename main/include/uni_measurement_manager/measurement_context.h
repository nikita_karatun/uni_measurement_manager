#pragma once

#include "uni_measurement_manager/enums.h"

#include <memory>
#include <vector>
#include <iostream>

#include "wx/string.h"
#include "wx/window.h"

namespace Uni_measurement_manager {

    class Application;

    class Measurement_device_factory;

    class Status_bar_aware_widget;

    class Measurement_payload;
    class Measurement_device_info_payload;
    class Drive_move_payload;

    class Measurement_settings;

    class Event_dispatcher;
    class Measurement_executor;

    class Main_frame;
    class Menu_bar;
    class Status_bar;
    class Plot_panel;
    class Input_panel;
    class Positioning_panel;
    class Settings_panel;
    class Output_panel;
    class Control_panel;
    class Buttons_panel;

    class Measurement_data {
        public:
            virtual ~Measurement_data() {}
            virtual void push_back(const Measurement_payload& p) = 0;
            virtual void clear() = 0;
            virtual void save(std::ostream&) = 0;
            virtual bool is_saved() = 0;
    };

    class Properties_provider;
    class Measurement_context {
        public:
            virtual ~Measurement_context() {}

            virtual void init() = 0;

            virtual Measurement_settings* settings() = 0;
            virtual Properties_provider* properties_provider() = 0;

            virtual void layout() = 0;
            virtual void enable_save_menu_item(bool enable) = 0;
            virtual void hide_controls() = 0;
            virtual void update_settings_from_ui() = 0;

            virtual void move_drive(int drive_id, Position_type::Enum, double position) = 0;
            virtual void step_position(bool increment = true) = 0;
            // void switch_drive(); todo to be implemented
            virtual void switch_position() = 0;

            virtual void toggle_measurement() = 0;
            virtual void cancel_measurement() = 0;

            virtual void set_status_bar_tooltip(Status_bar_aware_widget* widget, const wxString& tooltip) = 0;

            virtual void on_device_info_message(const Measurement_device_info_payload&) = 0;
            virtual void on_readout_message(const Measurement_payload&) = 0;
            virtual void on_drive_moved_message(const Drive_move_payload&) = 0;
            virtual void on_measurement_completed_message() = 0;
            virtual void on_paused_message() = 0;

            virtual void on_any_window_focus_set(wxWindow*) = 0;
    };

    class Readouts_writer;
    class Hot_keys_handler;
    class Ui_actors_factory;
    class Measurement_context_factory {
        public:
            virtual ~Measurement_context_factory() {}
            virtual std::auto_ptr<Measurement_context> create_measurement_context(Application*) = 0;
            virtual std::auto_ptr<Event_dispatcher> create_event_dispatcher(Measurement_context* measurement_context) = 0;
            virtual std::auto_ptr<Measurement_executor> create_measurement_executor(
                    Event_dispatcher* event_dispatcher, Measurement_device_factory*) = 0;
            virtual std::auto_ptr<Measurement_settings> create_measurement_settings() = 0;
            virtual std::auto_ptr<Measurement_data> create_measurement_data(Measurement_context*, Readouts_writer*) = 0;
            virtual std::auto_ptr<Hot_keys_handler> create_hot_keys_handler(Measurement_context*) = 0;
            virtual std::auto_ptr<Properties_provider> create_properties_provider() = 0;
            virtual std::auto_ptr<Readouts_writer> create_readouts_writer(Properties_provider*) = 0;
            virtual std::auto_ptr<Ui_actors_factory> create_ui_actors_factory(Measurement_context*) = 0;
    };

}
