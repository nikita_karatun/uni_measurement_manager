#pragma once

#include "uni_measurement_manager/iterator.h"

#include <memory>
#include <stdexcept>
#include <string>

namespace Uni_measurement_manager {

    class Measurement_device {
        public:
            class Drive {
                public:
                    typedef std::auto_ptr<Iterator<Drive*> > iterator_type;

                    virtual ~Drive() {}
                    virtual int id() = 0;
                    virtual double min_position() = 0;
                    virtual double max_position() = 0;
                    virtual void move(double position) = 0;
            };

            virtual ~Measurement_device() {}
            virtual void connect() = 0;
            virtual double read_out() = 0;
            virtual Drive::iterator_type drives_iterator() = 0;
    };

    class Measurement_device_exception : public std::runtime_error {
        public:
            Measurement_device_exception(std::string what) :std::runtime_error(what) {}
    };

    class Measurement_device_factory {
        public:
            virtual ~Measurement_device_factory() {}
            virtual std::auto_ptr<Measurement_device> create_measurement_device() = 0;
    };

}
