#pragma once

#include "uni_measurement_manager/properties_provider.h"

#include <map>
#include <string>

namespace Uni_measurement_manager {

    class Properties_provider_impl : public Properties_provider {
        public:
            Properties_provider_impl();

            std::string get_string(const std::string& key); // todo rename
            int get_int(const std::string& key);
            double get_double(const std::string& key);

            Properties_provider_impl* add_property(const std::string& key, const std::string& value);
            Properties_provider_impl* add_property(const std::string& key, int value);
            Properties_provider_impl* add_property(const std::string& key, double value);
        private:
            std::map<std::string, std::string> strings_;
            std::map<std::string, int> ints_;
            std::map<std::string, double> doubles_;

            template<typename T>
                T do_get_property(const typename std::map<std::string, T> map, const std::string& key, const T default_value);
    };

}
