#pragma once

#include "wx/panel.h"
#include <wx/statusbr.h>

namespace Uni_measurement_manager { // todo rename header

    const int DEFAULT_DRIVE_ID = 1; // todo delete

    class Measurement_context;

    class Abstract_panel : public wxPanel {
        public:
            Abstract_panel(wxWindow* parent, Measurement_context* measurement_context) 
                :wxPanel(parent), measurement_context_(measurement_context) 
            {}
            virtual void post_construct() {}
        protected:
            Measurement_context* measurement_context_;
    };

    class Status_bar : public wxStatusBar {
        public:
            Status_bar(wxWindow* parent, long style=wxSTB_DEFAULT_STYLE,
                    const wxString& name=wxStatusBarNameStr)
                :wxStatusBar(parent, wxID_ANY, style, name)
            {}
            virtual ~Status_bar() {}
            virtual void set_default_status_text(const wxString& text, int i = 0) = 0;
            virtual void set_default_status_text(int i = 0) = 0;
    };

    class Status_bar_aware_widget {
        public:
            virtual ~Status_bar_aware_widget() {}
            virtual void set_status_bar_tooltip(Uni_measurement_manager::Status_bar*, const wxString& tooltip) = 0;
            virtual void set_status_bar_tooltip(const wxString& tooltip) = 0;
    };

}

