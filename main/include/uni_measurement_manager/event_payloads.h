#pragma once

#include "uni_measurement_manager/enums.h"
#include <vector>

namespace Uni_measurement_manager {

    struct Measurement_payload {
        double x_;
        double y_;
        double y_av_;
        double error_;
        bool last_;
        Measurement_payload() {}
        Measurement_payload(double x, double y, double y_av, double error, bool last) 
            :x_(x), y_(y), y_av_(y_av), error_(error), last_(last) 
        {}
    };

    struct Drive_move_payload {
        int drive_id_;
        Position_type::Enum position_type_;
        double position_;
        bool successful_;
        Drive_move_payload() {}
        Drive_move_payload(int drive_id, Position_type::Enum position_type, double position, bool successful) 
            :drive_id_(drive_id), position_type_(position_type), position_(position), successful_(successful) 
        {}
    };

    struct Measurement_drive_info_payload {
        Measurement_drive_info_payload(int id, double min_position, double max_position)
            :id_(id), min_position_(min_position), max_position_(max_position)
        {}
        int id_;
        double min_position_;
        double max_position_;
    };

    struct Measurement_device_info_payload {
        Measurement_device_info_payload() {}
        Measurement_device_info_payload(std::vector<Measurement_drive_info_payload> drives)
            :drives_(drives)
        {}
        Measurement_device_info_payload& operator=(const Measurement_device_info_payload& o) {
            drives_ = o.drives_;
            return *this;
        };
        Measurement_device_info_payload(const Measurement_device_info_payload& o)
            :drives_(o.drives_)
        {}
        std::vector<Measurement_drive_info_payload> drives_;
    };

}
