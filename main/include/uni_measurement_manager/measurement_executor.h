#pragma once

#include "uni_measurement_manager/enums.h"

namespace Uni_measurement_manager {

    class Measurement_settings;

    class Measurement_executor {
        public:
            virtual ~Measurement_executor() {}
            virtual void start() = 0;
            virtual void pause() = 0;
            virtual void resume() = 0;
            virtual void stop() = 0;
            virtual void terminate() = 0; 
            virtual void join() = 0; 
            virtual void start_measurement(Measurement_settings*) = 0;
            virtual void move_drive(int drive_id, Position_type::Enum, double position) = 0;
    };

}
