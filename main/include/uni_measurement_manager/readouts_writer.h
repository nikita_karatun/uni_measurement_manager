#pragma once

#include <vector>
#include <string>

#include "uni_measurement_manager/event_payloads.h"

namespace Uni_measurement_manager {

    class Readouts_writer {
        public:
            virtual ~Readouts_writer() {}
            virtual std::string compose_file_name() = 0;
            virtual std::string file_mask() = 0;
            virtual void write(std::vector<Measurement_payload>&, std::ostream&) = 0;
    };

}
