#pragma once

#include "uni_measurement_manager/application.h"

#include <fstream>

namespace Uni_measurement_manager {

    class Measurement_context;

    class Application_impl : public Application {
        public:
            bool OnInit();
            int OnExit();
        protected:
            virtual std::auto_ptr<Measurement_context_factory> create_measurement_context_factory();
            virtual std::auto_ptr<Measurement_device_factory> create_measurement_device_factory() = 0;
        private:
            std::auto_ptr<Measurement_context> measurement_context_;
    };

}
