#pragma once

#include "uni_measurement_manager/measurement_context.h"

namespace Uni_measurement_manager {

    class Measurement_context_factory_impl : public Measurement_context_factory {
        public:
            Measurement_context_factory_impl();
            ~Measurement_context_factory_impl();
            std::auto_ptr<Measurement_context> create_measurement_context(Application*);
            std::auto_ptr<Event_dispatcher> create_event_dispatcher(Measurement_context* measurement_context);
            std::auto_ptr<Measurement_executor> create_measurement_executor(
                    Event_dispatcher* event_dispatcher, Measurement_device_factory*);
            std::auto_ptr<Measurement_settings> create_measurement_settings();
            std::auto_ptr<Measurement_data> create_measurement_data(Measurement_context*, Readouts_writer*);
            std::auto_ptr<Hot_keys_handler> create_hot_keys_handler(Measurement_context*);
            std::auto_ptr<Properties_provider> create_properties_provider();
            std::auto_ptr<Readouts_writer> create_readouts_writer(Properties_provider*);
            std::auto_ptr<Ui_actors_factory> create_ui_actors_factory(Measurement_context*);
    };

}
