set(CMAKE_CXX_STANDARD 98)

set(SOURCES_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../src)

include_directories(PRIVATE 
    ${CMAKE_CURRENT_SOURCE_DIR}/../src ${CMAKE_CURRENT_SOURCE_DIR}/../include)
link_libraries(GTest::GTest GTest::Main)

add_executable(settings_repository_test settings_repository_test.cpp
    ${SOURCES_DIR}/settings_repository.cpp
    ${SOURCES_DIR}/database.cpp
    ${SOURCES_DIR}/sqlite_wrapper.cpp
    ${SOURCES_DIR}/measurement_settings_impl.cpp)
target_include_directories(settings_repository_test PRIVATE ${SQLite3_INCLUDE_DIRS})
target_link_libraries(settings_repository_test ${SQLite3_LIBRARIES})
do_add_test(settings_repository_test)

add_executable(measurement_task_test measurement_task_test.cpp 
    ${SOURCES_DIR}/measurement_task.cpp)
target_link_libraries(measurement_task_test ${wxWidgets_LIBRARIES})
do_add_test(measurement_task_test)

add_executable(readouts_writer_test readouts_writer_test.cpp
    ${SOURCES_DIR}/readouts_writer_impl.cpp
    ${SOURCES_DIR}/properties_provider_impl.cpp)
target_link_libraries(readouts_writer_test)
do_add_test(readouts_writer_test)

