#include "gtest/gtest.h"

#include <cstdio>
#include <iostream>
#include <sstream>

#include "uni_measurement_manager/enums.h"
#include "uni_measurement_manager/event_payloads.h"

#include "task.h"
#include "measurement_task.h"

namespace Uni_measurement_manager {

    class Mock_measurement_device : public Measurement_device {
        public:
            class Mock_drive : public Measurement_device::Drive {
                int id() { return 1; };
                double min_position() { return 1.1; }
                double max_position() { return 1.5; }
                void move(double position) {}
            };
            ~Mock_measurement_device() {
                for (std::vector<Drive*>::iterator it = drives_.begin(); it != drives_.end(); ++it)
                    delete *it;
            }
            void connect() {};
            double read_out() { return 2.2; }
            Drive::iterator_type drives_iterator() { return Drive::iterator_type(new Iterator_impl<std::vector<Measurement_device::Drive*> >(drives_)); }
            Mock_measurement_device() :drives_() {
				drives_.push_back(new Mock_drive);
			}
        private:
            std::vector<Measurement_device::Drive*> drives_;
    };

    class Measurement_device_info_payload;
    class Mock_event_dispatcher : public Event_dispatcher {
        public:
            void send_device_info_message(const Measurement_device_info_payload&) {}
            void send_readout_message(const Measurement_payload& p) {
                stream_ << 
                    "sample: " << sample_ << ", " <<
                    "x: " << p.x_ << ", " <<
                    "y: " << p.y_ << ", " <<
                    "average: " << p.y_av_ << ", " <<
                    "error: " << p.error_ << ", " <<
                    "last: " << p.last_ <<
                    std::endl;
                if (p.last_) sample_ = 1;
                else ++sample_;
            }
            void send_drive_moved_message(const Drive_move_payload&) {}
            void send_measurement_completed_message() {}
            void send_paused_message() {}
            void send_device_error_message(std::string message) {}

            std::string output() { return stream_.str(); }
			Mock_event_dispatcher() :sample_(1) {}
        private:
            int sample_;
            std::ostringstream stream_;
    };

    class Mock_facilities {
        public:
            Measurement_device* measurement_device() { return measurement_device_; }
            Event_dispatcher* event_dispatcher() { return event_dispatcher_; }
            Mock_facilities(Measurement_device* measurement_device, Event_dispatcher* event_dispatcher)
                :measurement_device_(measurement_device), event_dispatcher_(event_dispatcher)
            {}
        private:
            Measurement_device* measurement_device_;
            Event_dispatcher* event_dispatcher_;
    };

    struct Mock_measurement_executor {
        typedef Mock_facilities facilities_type;
        void pause() {}
        void wait_while_paused() {}
        bool is_terminated_or_stopped() { return false; }
        void sleep(long ms) {}
    };
}

using namespace Uni_measurement_manager;

TEST(Measurement_task_test, test_1) {
    std::auto_ptr<Mock_event_dispatcher> event_dispatcher(new Mock_event_dispatcher);

    Measurement_parameters parameters;
    parameters.begin_position_ = 1;
    parameters.end_position_ = 1.04;
    parameters.step_ = 0.01;
    parameters.error_ = 0.01;
    parameters.min_samples_ = 2;
    parameters.max_samples_ = 30;

    Measurement_task<Mock_measurement_executor> task(event_dispatcher.get(), parameters);

    std::auto_ptr<Measurement_device> measurement_device(new Mock_measurement_device);
    Mock_facilities facilities(measurement_device.get(), event_dispatcher.get());
    Mock_measurement_executor measurement_executor;
    task(measurement_executor, facilities);

    std::string expected;
    expected
        .append("sample: 1, x: 1, y: 2.2, average: 2.2, error: -1, last: 0\n")
        .append("sample: 2, x: 1, y: 2.2, average: 2.2, error: 0, last: 1\n")
        .append("sample: 1, x: 1.01, y: 2.2, average: 2.2, error: -1, last: 0\n")
        .append("sample: 2, x: 1.01, y: 2.2, average: 2.2, error: 0, last: 1\n")
        .append("sample: 1, x: 1.02, y: 2.2, average: 2.2, error: -1, last: 0\n")
        .append("sample: 2, x: 1.02, y: 2.2, average: 2.2, error: 0, last: 1\n")
        .append("sample: 1, x: 1.03, y: 2.2, average: 2.2, error: -1, last: 0\n")
        .append("sample: 2, x: 1.03, y: 2.2, average: 2.2, error: 0, last: 1\n")
        .append("sample: 1, x: 1.04, y: 2.2, average: 2.2, error: -1, last: 0\n")
        .append("sample: 2, x: 1.04, y: 2.2, average: 2.2, error: 0, last: 1\n");

    ASSERT_EQ(event_dispatcher->output(), expected);
}
