#include "gtest/gtest.h"

#include <memory>
#include <sstream>
#include <vector>

#include "readouts_writer_impl.h"
#include "uni_measurement_manager/properties_provider_impl.h"
#include "uni_measurement_manager/event_payloads.h"

using namespace Uni_measurement_manager;

TEST(Readouts_writer, test_1) {
    std::vector<Measurement_payload> readouts;
    readouts.push_back(Measurement_payload(1, 2, 2, 0.01, true));
    readouts.push_back(Measurement_payload(2, 2, 2, 0.01, true));
    readouts.push_back(Measurement_payload(3, 2, 2, 0.01, true));
    std::auto_ptr<Properties_provider> properties_provider(new Properties_provider_impl);

    std::auto_ptr<Readouts_writer> writer(new Readouts_writer_impl(properties_provider.get()));
    std::stringstream stream;
    stream << writer->file_mask() << std::endl;
    writer->write(readouts, stream);

    std::string expected;
    expected
        .append("*.csv\n")
        .append("Position,Value,Error\n")
        .append("1,2,0.01\n")
        .append("2,2,0.01\n")
        .append("3,2,0.01\n");

    ASSERT_EQ(stream.str(), expected);
}
