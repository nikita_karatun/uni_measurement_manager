#include "gtest/gtest.h"

#include "measurement_settings_impl.h"
#include "settings_repository.h"

#include <iostream>

using namespace Uni_measurement_manager;

std::auto_ptr<Database> create_database() {
    return std::auto_ptr<Database>(new Database("settings_test.db", true));
}

std::auto_ptr<Measurement_settings> create_settings() {
    std::auto_ptr<Database> database = create_database();
    std::auto_ptr<Settings_repository> settings_repository(new Settings_repository(*database.get()));
    return std::auto_ptr<Measurement_settings>(new Measurement_settings_impl(database, settings_repository));
}

TEST(Database, test1) {
    {
        std::auto_ptr<Database> database = create_database();
        database->prepare_statement("DROP TABLE `settings`").execute();
        database->prepare_statement("DROP TABLE `drive`").execute();
    }
    {
        std::auto_ptr<Measurement_settings> settings = create_settings();
        settings->load();
        for (int i = 1; i < 4; ++i) {
            Measurement_settings::Drive* drive = settings->create_drive(i);
            drive->set_default_values();
        }
        settings->save();
    }
    {
        std::auto_ptr<Measurement_settings> settings = create_settings();
        settings->load();
        ASSERT_EQ(settings->step(), 0.01);
        ASSERT_EQ(settings->averaging_time(), 300);
        ASSERT_EQ(settings->min_samples(), 5);
        ASSERT_EQ(settings->max_samples(), 30);
        ASSERT_EQ(settings->error(), 0.1);

        int i = 1;
        for (Measurement_settings::Drive::iterator_type it = settings->drives_iterator(); it->has_next(); it->next()) {
            Measurement_settings::Drive* drive = it->get();
            ASSERT_EQ(drive->id(), i);
            ASSERT_EQ(drive->begin_position(), 1);
            ASSERT_EQ(drive->end_position(), 2);
            ASSERT_EQ(drive->step(), 0.01);
            ++i;
        }
        Measurement_settings::Drive* drive = settings->get_or_create_drive_by_id(1);
        drive->set_begin_position(3);
        drive->set_end_value(5);
        drive->set_step(0.5);
        settings->save();
    }
    {
        std::auto_ptr<Measurement_settings> settings = create_settings();
        settings->load();
        Measurement_settings::Drive* drive = settings->get_or_create_drive_by_id(1);
        ASSERT_EQ(drive->id(), 1);
        ASSERT_EQ(drive->begin_position(), 3);
        ASSERT_EQ(drive->end_position(), 5);
        ASSERT_EQ(drive->step(), 0.5);
    }
}
