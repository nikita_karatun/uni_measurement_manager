#include "uni_measurement_manager/measurement_settings.h"

#include "stdafx.h"
#include "settings_repository.h"
#include "sqlite_wrapper.h"

#include <iostream>
#include <set>

using Sqlite_wrapper::Result_set;

namespace Uni_measurement_manager {

    struct Settings_mapper {
        Measurement_settings* settings_;
        bool found_;
        Settings_mapper(Measurement_settings* settings) :settings_(settings), found_(false) {}
        void operator()(Result_set result_set) {
            found_ = true;

            int settings_id = result_set.get_next<int>();
            double step = result_set.get_next<double>();
            int averaging_time = result_set.get_next<int>();
            int min_samples = result_set.get_next<int>();
            int max_samples = result_set.get_next<int>();
            double error = result_set.get_next<double>();

            settings_->set_id(settings_id);
            settings_->set_step(step);
            settings_->set_averaging_time(averaging_time);
            settings_->set_min_samples(min_samples);
            settings_->set_max_samples(max_samples);
            settings_->set_error(error);
        }
    };

    struct Drives_mapper {
        Measurement_settings* settings_;
        Drives_mapper(Measurement_settings* settings) :settings_(settings) {}
        void operator()(Result_set result_set) {
            int drive_id = result_set.get_next<int>();
            double begin_position = result_set.get_next<double>();
            double end_position = result_set.get_next<double>();
            double step = result_set.get_next<double>();
            Measurement_settings::Drive* drive = settings_->create_drive(drive_id);
            drive->set_begin_position(begin_position);
            drive->set_end_value(end_position);
            drive->set_step(step);
        }
    };

    Settings_repository::Settings_repository(Database& database) 
        :select_settings_statement_(database.prepare_statement(
                    "select id, step, averaging_time, min_samples, max_samples, error \
                    from settings order by id desc limit 1")), 
        insert_settings_statement_(database.prepare_statement(
                    "insert into settings \
                    (step, averaging_time, min_samples, max_samples, error) \
                    values (?, ?, ?, ?, ?)")),
        update_statement_(database.prepare_statement(
                    "update settings set \
                    step = ?, averaging_time = ?, min_samples = ?, max_samples = ?, error = ?")),

        select_drives_statement_(database.prepare_statement(
                    "select id, begin_position, end_position, step \
                    from drive where settings_id = ?")),
        select_drive_ids_statement_(database.prepare_statement(
                    "select id from drive where settings_id = ?")),
        insert_drive_statement_(database.prepare_statement(
                    "insert into drive \
                    (id, settings_id, begin_position, end_position, step) \
                    values (?, ?, ?, ?, ?)")),
        update_drive_statement_(database.prepare_statement(
                    "update drive set \
                    begin_position = ?, end_position = ?, step = ? \
                    where id = ? and settings_id = ?"))
        {}

    void Settings_repository::load(Measurement_settings* settings) {
        Settings_mapper settings_mapper(settings);
        select_settings_statement_.query(settings_mapper);
        if (settings_mapper.found_) {
            Drives_mapper drives_mapper(settings);
            select_drives_statement_
                .bind_next(settings->id())
                .query(drives_mapper);
        } else {
            settings->set_default_values();
            insert_settings_statement_
                .bind_next(settings->step())
                .bind_next(settings->averaging_time())
                .bind_next(settings->min_samples())
                .bind_next(settings->max_samples())
                .bind_next(settings->error())
                .execute();
            select_settings_statement_.query(settings_mapper);
        }
    }

    void Settings_repository::save(Measurement_settings* settings) {
        update_statement_
            .bind_next(settings->step())
            .bind_next(settings->averaging_time())
            .bind_next(settings->min_samples())
            .bind_next(settings->max_samples())
            .bind_next(settings->error())
            .execute();

        std::set<int> stored_ids = select_drive_ids_statement_ 
            .bind_next(settings->id())
            .query_to_container<std::set<int> >();

        for (Measurement_settings::Drive::iterator_type it = settings->drives_iterator(); it->has_next(); it->next()) {
            Measurement_settings::Drive* drive = it->get();
            if (stored_ids.find(drive->id()) == stored_ids.end())
                insert_drive_statement_
                    .bind_next(drive->id())
                    .bind_next(settings->id())
                    .bind_next(drive->begin_position())
                    .bind_next(drive->end_position())
                    .bind_next(drive->step())
                    .execute();
            else
               update_drive_statement_ 
                    .bind_next(drive->begin_position())
                    .bind_next(drive->end_position())
                    .bind_next(drive->step())
                    .bind_next(drive->id())
                    .bind_next(settings->id())
                    .execute();
        } // todo delete no longer existing drives
    }

}
