#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "uni_measurement_manager/common.h"

#include <map>

namespace Uni_measurement_manager {

    class Status_bar_impl : public Status_bar {
        public:
            Status_bar_impl(wxWindow* parent, long style=wxSTB_DEFAULT_STYLE,
                    const wxString& name=wxStatusBarNameStr);
            void set_default_status_text(const wxString& text, int i = 0);
            void set_default_status_text(int i = 0);
        private:
            std::map<int, wxString> default_values_; // todo use Pop and Push default status bar methods
    };

    template<typename E>
        class Status_bar_aware_widget_impl : public Status_bar_aware_widget, public E {
            public:
                Status_bar_aware_widget_impl() :status_bar_(NULL) {}

                void set_status_bar_tooltip(Status_bar* status_bar, const wxString& tooltip) {
                    status_bar_ = status_bar;
                    set_status_bar_tooltip(tooltip);
                    E::Bind(wxEVT_ENTER_WINDOW, &Status_bar_aware_widget_impl::on_mouse_enter, this);
                    E::Bind(wxEVT_LEAVE_WINDOW, &Status_bar_aware_widget_impl::on_mouse_leave, this);
                }

                void set_status_bar_tooltip(const wxString& tooltip) {
                    status_bar_tooltip_ = tooltip;
                }
            private:
                Status_bar* status_bar_;
                wxString status_bar_tooltip_;

                void on_mouse_enter(wxMouseEvent& event) {
                    if (status_bar_)
                        status_bar_->PushStatusText(status_bar_tooltip_);
                    event.Skip();
                }

                void on_mouse_leave(wxMouseEvent& event) {
                    if (status_bar_)
                        status_bar_->PopStatusText();
                    event.Skip();
                }
        };

}
