#include "stdafx.h"
#include "settings_panel_impl.h"

#include "wx_custom.h"

#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/measurement_settings.h"
#include "uni_measurement_manager/properties_provider.h"
#include "uni_measurement_manager/event_dispatcher.h"
#include "uni_measurement_manager/event_payloads.h"

namespace Uni_measurement_manager {

    class Fields_composer {
        private:
            wxWindow* window_;
            wxSizer* sizer_;
            int border_size_;
        public:
            Fields_composer(wxWindow* window, wxSizer* sizer, int border_size) :window_(window), sizer_(sizer), border_size_(border_size) {}
            template<typename T>
                T* add_field(wxString label, T* item) {
                    sizer_->Add(new wxStaticText(window_, wxID_ANY, label), 0, wxALIGN_CENTER_VERTICAL | wxRIGHT, border_size_);
                    sizer_->Add(item, 1, wxALIGN_CENTER_VERTICAL | wxEXPAND);
                    return item;
                }
            wxSpinCtrl* compose_int_ctrl(int value) {
                return new wxSpinCtrl(window_, wxID_ANY, Wx_custom::String::to_string(value),
                        wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0, INT_MAX);
            }
            wxSpinCtrlDouble* compose_double_ctrl(double value, double min, double max, double increment) {
                return new wxSpinCtrlDouble(window_, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 
                        min, max, value, increment); // todo hardcoded
            }
    };

    Settings_panel_impl::Settings_panel_impl(wxWindow* parent, Measurement_context* measurement_context) 
        :Settings_panel(parent, measurement_context)
    {}

    void Settings_panel_impl::on_device_info_message(const Measurement_device_info_payload& device) {

        const std::vector<Measurement_drive_info_payload>& drives = device.drives_;
        std::vector<Measurement_drive_info_payload>::const_iterator it = drives.begin();

        double min_position;
        double max_position;
        if (it != drives.end()) {
            min_position = it->min_position_;
            max_position = it->max_position_;
            ++it;
            for (; it != drives.end(); ++it) {
                min_position = std::max(min_position, it->min_position_);
                max_position = std::min(min_position, it->max_position_);
            }
        } else {
            min_position = 0;
            max_position = 10;
        }

        wxFlexGridSizer* sizer = new wxFlexGridSizer(2, 0, 0);
        int border_size = measurement_context_->properties_provider()->get_int("window.formatting.border_size");
        Fields_composer fc(this, sizer, border_size);

        Measurement_settings* settings = measurement_context_->settings();

        double increment = measurement_context_->properties_provider()->get_double("window.formatting.double_spinner.increment");
        step_field_ = fc.add_field("Step", fc.compose_double_ctrl(settings->step(), 0, max_position - min_position, increment)); // todo hardcoded
        averaging_time_field_ = fc.add_field("Av. time (ms)", fc.compose_int_ctrl(settings->averaging_time()));
        min_samples_field_ = fc.add_field("Min samples", fc.compose_int_ctrl(settings->min_samples()));
        max_samples_field_ = fc.add_field("Max samples", fc.compose_int_ctrl(settings->max_samples()));
        error_field_ = fc.add_field("Error", fc.compose_double_ctrl(settings->error(), 0, 1, increment));

        sizer->AddGrowableCol(0, 1);

        wxStaticBoxSizer* static_box_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this, "Settings"); // todo hardcoded, search for all string literals in project and replace with properties
        static_box_sizer->Add(sizer, 1, wxEXPAND | wxALL, border_size);
        SetSizer(static_box_sizer);
    }

    void Settings_panel_impl::write_settings() {
        Measurement_settings* settings = measurement_context_->settings();
        settings->set_step(step_field_->GetValue());
        settings->set_averaging_time(averaging_time_field_->GetValue());
        settings->set_min_samples(min_samples_field_->GetValue());
        settings->set_max_samples(max_samples_field_->GetValue());
        settings->set_error(error_field_->GetValue());
    }

}
