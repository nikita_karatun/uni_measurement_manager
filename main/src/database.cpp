#include "stdafx.h"

#include "database.h"

namespace Uni_measurement_manager {

    Database::Database(const char* filepath, bool trace)
        :Sqlite_wrapper::Database(filepath, trace)
    {
        prepare_statement(
                "CREATE TABLE IF NOT EXISTS `settings` ( \
                `id`	            INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \
                `step`	            DECIMAL(10,5) NOT NULL, \
                `averaging_time`	INTEGER NOT NULL, \
                `min_samples`	    INTEGER NOT NULL, \
                `max_samples`	    INTEGER NOT NULL, \
                `error`	            DECIMAL(10,5) NOT NULL \
                );")
            .execute();
        prepare_statement(
                "CREATE TABLE IF NOT EXISTS `drive` ( \
                `id`	        INTEGER NOT NULL, \
                `settings_id`	    INTEGER NOT NULL, \
                `begin_position`    DECIMAL(10,5) NOT NULL, \
                `end_position`      DECIMAL(10,5) NOT NULL, \
                `step`	            DECIMAL(10,5) NOT NULL, \
                foreign key(settings_id) references settings(id) \
                primary key(id, settings_id) \
                );")
            .execute();
    }

}
