#include "uni_measurement_manager/hot_keys_handler_impl.h"

#include <iostream>

namespace Uni_measurement_manager {

    struct Switch_position_handler : public Hot_key_handler {
        void handle(Measurement_context* measurement_context) {
            measurement_context->switch_position();
        }
    };

    struct Resume_measurement_handler : public Hot_key_handler {
        void handle(Measurement_context* measurement_context) {
            measurement_context->toggle_measurement();
        }
    };

    struct Cancel_measurement_handler : public Hot_key_handler {
        void handle(Measurement_context* measurement_context) {
            measurement_context->cancel_measurement();
        }
    };

    struct Increase_step_handler : public Hot_key_handler {
        void handle(Measurement_context* measurement_context) {
            measurement_context->step_position();
        }
    };

    struct Decrease_step_handler : public Hot_key_handler {
        void handle(Measurement_context* measurement_context) {
            measurement_context->step_position(false);
        }
    };

    Hot_keys_handler_impl::Hot_keys_handler_impl(Measurement_context* measurement_context) 
        :Hot_keys_handler(measurement_context)
    {
        map_[Hot_key(true, true, wxKeyCode('S'))] = new Switch_position_handler;
        map_[Hot_key(true, true, wxKeyCode('B'))] = new Resume_measurement_handler;
        map_[Hot_key(true, true, wxKeyCode('C'))] = new Cancel_measurement_handler;
        map_[Hot_key(true, false, wxKeyCode('Z'))] = new Decrease_step_handler;
        map_[Hot_key(true, false, wxKeyCode('X'))] = new Increase_step_handler;
    }

    Hot_keys_handler_impl::~Hot_keys_handler_impl() {
        for (std::map<Hot_key, Hot_key_handler*>::iterator it = map_.begin(); it != map_.end(); ++it)
            delete it->second;
    }

    void Hot_keys_handler_impl::add_hot_key_handler(Hot_key hot_key, Hot_key_handler* hot_key_handler) {
        std::map<Hot_key, Hot_key_handler*>::iterator it = map_.find(hot_key);
        if (it != map_.end()) {
            delete it->second;
            it->second = hot_key_handler;
        } else
            map_[hot_key] = hot_key_handler;
        
    }

    void Hot_keys_handler_impl::connect(wxEvtHandler* event_handler) {
        event_handler->Bind(wxEVT_KEY_DOWN, &Hot_keys_handler_impl::on_key_down, this);
        event_handler->Bind(wxEVT_SET_FOCUS, &Hot_keys_handler_impl::on_focus_set, this);
    }

    void Hot_keys_handler_impl::connect_recursively(wxWindow* component) {
        if (component) {
            connect(component);
            wxWindowListNode* child_node = component->GetChildren().GetFirst();
            while (child_node) {
                wxWindow* child = child_node->GetData();
                connect_recursively(child);
                child_node = child_node->GetNext();
            }
        }
    }

    void Hot_keys_handler_impl::on_key_down(wxKeyEvent& event) {
        std::map<Hot_key, Hot_key_handler*>::iterator it = map_.find(
                Hot_key(event.ControlDown(), event.ShiftDown(), wxKeyCode(event.GetKeyCode())));
        if (it != map_.end())
            it->second->handle(measurement_context_);
        else
            event.Skip();
    }

    void Hot_keys_handler_impl::on_focus_set(wxFocusEvent& event) {
        measurement_context_->on_any_window_focus_set(event.GetWindow()); // todo delete?
        event.Skip();
    }

}
