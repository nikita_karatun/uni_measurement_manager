#include "measurement_context_impl.h"

#include "uni_measurement_manager/application.h"
#include "uni_measurement_manager/measurement_executor.h"
#include "uni_measurement_manager/measurement_settings.h"
#include "uni_measurement_manager/hot_keys_handler.h"
#include "uni_measurement_manager/readouts_writer.h"
#include "uni_measurement_manager/panels.h"
#include "uni_measurement_manager/positioning_panel.h"

#include <fstream>

#include "wx/filedlg.h"

#include "ui_actors_factory_impl.h"

namespace Uni_measurement_manager {

    Measurement_context_impl::Measurement_context_impl(Application* application, Measurement_context_factory* factory) 
        :
            application_(application),
            measurement_device_factory_(application->create_measurement_device_factory()),
            event_dispatcher_(factory->create_event_dispatcher(this)),
            measurement_executor_(factory->create_measurement_executor(event_dispatcher_.get(), measurement_device_factory_.get())),
            measurement_settings_(factory->create_measurement_settings()),
            hot_key_handler_(factory->create_hot_keys_handler(this)),
            properties_provider_(factory->create_properties_provider()),
            readouts_writer_(factory->create_readouts_writer(properties_provider_.get())),
            measurement_data_(factory->create_measurement_data(this, readouts_writer_.get())),
            ui_actors_(factory->create_ui_actors_factory(this)->create_ui_actors()),

            main_frame_(ui_actors_.main_frame_),
            menu_bar_(ui_actors_.menu_bar_), 
            status_bar_(ui_actors_.status_bar_),
            plot_panel_(ui_actors_.plot_panel_),
            input_panel_(ui_actors_.input_panel_),
            positioning_panel_(ui_actors_.positioning_panel_),
            settings_panel_(ui_actors_.settings_panel_),
            output_panel_(ui_actors_.output_panel_),
            buttons_panel_(ui_actors_.buttons_panel_),

            state_controller_(this) {}

    void Measurement_context_impl::init() {
        measurement_executor_->start();

        main_frame_->post_construct();
        plot_panel_->post_construct();
        input_panel_->post_construct();
        positioning_panel_->post_construct();
        settings_panel_->post_construct();
        output_panel_->post_construct();
        buttons_panel_->post_construct();

        status_bar_->set_default_status_text("Ready");
        main_frame_->Bind(wxEVT_MENU, &Measurement_context_impl::on_save, this, wxID_SAVE);
        main_frame_->Bind(wxEVT_CLOSE_WINDOW, &Measurement_context_impl::on_close, this);
        hot_key_handler_->connect(application_);
        hot_key_handler_->connect_recursively(main_frame_);
        main_frame_->Show();
    }

    void Measurement_context_impl::layout() {
        main_frame_->Layout();
    }

    void Measurement_context_impl::enable_save_menu_item(bool enable) {
        menu_bar_->enable_save_item(enable);
    }

    void Measurement_context_impl::hide_controls() {
        input_panel_->Hide();
        output_panel_->render_settings();
        output_panel_->Show();
        main_frame_->Layout();
        output_panel_->SetFocus();
    }

    void Measurement_context_impl::update_settings_from_ui() {
        positioning_panel_->write_settings();
        settings_panel_->write_settings();
    }

    void Measurement_context_impl::move_drive(int drive_id, Position_type::Enum position_type, double position) {
        state_controller_.set_current(state_controller_.pending());
        measurement_executor_->move_drive(drive_id, position_type, position);
    }

    void Measurement_context_impl::step_position(bool increment) {
        state_controller_->step_position(increment);
    }

    void Measurement_context_impl::switch_position() {
        state_controller_->switch_position();
    }

    void Measurement_context_impl::toggle_measurement() {
        state_controller_->toggle_measurement();
    }

    void Measurement_context_impl::cancel_measurement() {
        state_controller_->cancel_measurement();
    }

    void Measurement_context_impl::set_status_bar_tooltip(Status_bar_aware_widget* widget, const wxString& tooltip) {
        widget->set_status_bar_tooltip(status_bar_, tooltip);
    }

    void Measurement_context_impl::save_readouts() {
        std::string file_name = readouts_writer_->compose_file_name();
        std::string file_mask = readouts_writer_->file_mask();
        wxFileDialog file_dialog(main_frame_, wxFileSelectorPromptStr, wxEmptyString, file_name, file_mask, wxFD_SAVE);
        if (file_dialog.ShowModal() == wxID_OK) {
            std::ofstream stream;
            stream.open(file_dialog.GetPath().ToStdString().c_str(), std::ios::out);
            measurement_data_->save(stream);
        }
    }

    void Measurement_context_impl::on_device_info_message(const Measurement_device_info_payload& device_info) {
        state_controller_.set_current(state_controller_.stopped());
        positioning_panel_->on_device_info_message(device_info);
        settings_panel_->on_device_info_message(device_info);
        layout();
    }

    void Measurement_context_impl::on_readout_message(const Measurement_payload& p) {
        state_controller_->on_readout_message(p);
    }

    void Measurement_context_impl::on_drive_moved_message(const Drive_move_payload& p) {
        state_controller_.set_current(state_controller_.stopped());
        positioning_panel_->on_drive_moved_message(p);
        layout();
    }

    void Measurement_context_impl::on_measurement_completed_message() {
        state_controller_.set_current(state_controller_.stopped());

        output_panel_->Hide();
        output_panel_->clear();
        input_panel_->Show();
        input_panel_->SetFocus();
        layout();
        status_bar_->set_default_status_text("Ready");
    }

    void Measurement_context_impl::on_paused_message() {
        state_controller_.set_current(state_controller_.paused());
        status_bar_->set_default_status_text("Paused");
    }

    void Measurement_context_impl::on_any_window_focus_set(wxWindow* window) {
        // positioning_panel_->on_any_window_focus_set(window); todo delete
    }

    void Measurement_context_impl::on_save(wxCommandEvent&) {
        save_readouts();
    }

    void Measurement_context_impl::on_close(wxCloseEvent& e) {
        measurement_executor_->terminate();
        update_settings_from_ui();
        measurement_settings_->save();
        measurement_executor_->join();
        e.Skip();
    }

}
