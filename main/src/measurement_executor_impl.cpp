#include "stdafx.h"
#include "measurement_executor_impl.h"

#include <sstream>

#include "measurement_task.h"
#include "uni_measurement_manager/measurement_settings.h"
#include "uni_measurement_manager/event_dispatcher.h"
#include "uni_measurement_manager/event_payloads.h"

namespace Uni_measurement_manager {

    Facilities_composer::Facilities_composer(Event_dispatcher* event_dispatcher,
            Measurement_device_factory* measurement_device_factory) 
        :event_dispatcher_(event_dispatcher),
        measurement_device_factory_(measurement_device_factory) 
    {}

    Facilities Facilities_composer::compose() {
        // wxThread::Sleep(1000); // todo remove
        Facilities facilities(event_dispatcher_, measurement_device_factory_->create_measurement_device().release());

        Measurement_device* measurement_device = facilities.measurement_device();
        std::vector<Measurement_drive_info_payload> drives;
        for (Measurement_device::Drive::iterator_type it = measurement_device->drives_iterator(); it->has_next(); it->next()) {
            Measurement_device::Drive* drive = it->get();
            drives.push_back(Measurement_drive_info_payload(drive->id(), drive->min_position(), drive->max_position()));
        }
        Measurement_device_info_payload device_info(drives);
        facilities.event_dispatcher()->send_device_info_message(device_info);

        try {
            facilities.measurement_device()->connect();
        } catch (const Measurement_device_exception& e) {
            event_dispatcher_->send_device_error_message(e.what());
        }
        return facilities;
    }

    Facilities::Facilities(Event_dispatcher* event_dispatcher, Measurement_device* measurement_device)
        :event_dispatcher_(event_dispatcher), measurement_device_(measurement_device)
    {
        for (Measurement_device::Drive::iterator_type it = measurement_device->drives_iterator(); it->has_next(); it->next())
            drives_[it->get()->id()] = it->get();
    }

    Facilities::Facilities(const Facilities& o) 
        :event_dispatcher_(o.event_dispatcher_), measurement_device_(o.measurement_device_), drives_(o.drives_)
    {
        o.measurement_device_ = NULL;
    }

    Facilities::~Facilities() {
        delete measurement_device_;
    }

    Measurement_device::Drive* Facilities::get_drive_by_id(int id) { 
        std::map<int, Measurement_device::Drive*>::iterator it = drives_.find(id);
        if (it == drives_.end()) {
            std::stringstream ss;
            ss << "Drive not found, id = ";
            ss << id;
            throw Measurement_device_exception(ss.str());
        }
        return it->second;
    }

    struct Move_drive_task : Single_thread_executor<Facilities_composer>::task_type {
        int drive_id_;
        Position_type::Enum position_type_;
        double position_;
        Move_drive_task(int drive_id, Position_type::Enum position_type, double position)
            :drive_id_(drive_id), position_type_(position_type), position_(position)
        {}
        void operator()(Single_thread_executor<Facilities_composer>& ex, Facilities& facilities) {
            if (!ex.is_terminated_or_stopped()) {
                // wxThread::Sleep(300); // todo remove
                std::string error_message;
                try {
					Measurement_device::Drive* drive = facilities.get_drive_by_id(drive_id_);
                    drive->move(position_);
                } catch (const Measurement_device_exception& e) {
                    error_message = e.what();
                }
                // device moved message sent to unlock position control of position panel
                // position panel state is rolled back in case of device error
                facilities.event_dispatcher()->send_drive_moved_message(
                        Drive_move_payload(drive_id_, position_type_, position_, error_message.empty()));
                if (!error_message.empty())
                    facilities.event_dispatcher()->send_device_error_message(error_message);
            }
        }
    };

    Measurement_executor_impl::Measurement_executor_impl(
            Event_dispatcher* event_dispatcher, Measurement_device_factory* measurement_device_factory) 
        :Single_thread_executor(Facilities_composer(event_dispatcher, measurement_device_factory), false),
        event_dispatcher_(event_dispatcher)
    {}

    void Measurement_executor_impl::move_drive(int drive_id, Position_type::Enum position_type, double position) {
        Move_drive_task task(drive_id, position_type, position);
        execute(task);
    }

    void Measurement_executor_impl::start_measurement(Measurement_settings* settings) { // todo Measurement_settings * const settings ?
        Measurement_parameters parameters;
        parameters.step_ = settings->step();
        parameters.averaging_time_ = settings->averaging_time();
        parameters.min_samples_ = settings->min_samples();
        parameters.max_samples_ = settings->max_samples();
        parameters.error_ = settings->error();

        Measurement_settings::Drive::iterator_type it = settings->drives_iterator();
        Measurement_settings::Drive* drive = it->get(); // todo select drive
        parameters.begin_position_ = drive->begin_position();
        parameters.end_position_ = drive->end_position();

        Measurement_task<Single_thread_executor<Facilities_composer> > task(event_dispatcher_, parameters);
        execute(task);
    }

}
