#pragma once 

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "help_frame.h"

#include "uni_measurement_manager/panels.h"

class wxAnimationCtrl;

namespace Uni_measurement_manager {

    class Main_frame_impl : public Main_frame {
        public:
            Main_frame_impl(Measurement_context* measurement_context);
        private:
            Measurement_context* measurement_context_;
            Help_frame* help_frame_;
            void on_exit(wxCommandEvent&);
            void on_help(wxCommandEvent&);
    };

}
