#pragma once

#include "wx/panel.h"
#include "wx_simple_plot.h"

#include "uni_measurement_manager/panels.h"

namespace Uni_measurement_manager {

    class Plot_panel_impl : public Plot_panel {
        public:
            Plot_panel_impl(wxWindow* parent, Measurement_context*);
            void set_default_axes();
            void clear(Data_set::e data_set = Data_set::ALL);
            void add_point(double x, double y, Data_set::e data_set = Data_set::ALL);
            void set_x_limits(double begin, double end);
        private:
            Spectrometer::SimplePlot* plot_;
    };

}
