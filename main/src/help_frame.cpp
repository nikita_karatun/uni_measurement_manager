#include "stdafx.h"
#include "help_frame.h"

#include "wx/html/htmlwin.h"

Help_frame::Help_frame(wxWindow* parent) :wxFrame(parent, wxID_ANY, "Help") {
    wxHtmlWindow* html = new wxHtmlWindow(this, wxID_ANY, wxDefaultPosition, wxSize(380, 160), wxHW_SCROLLBAR_NEVER);
    html->SetBorders(0);
    html->AppendToPage(
            "<html> \
            <body> \
            <h1>test</h1> \
            <p>test</p> \
            </body> \
            </html>");
    html->SetSize(html -> GetInternalRepresentation() -> GetWidth(),
                    html -> GetInternalRepresentation() -> GetHeight());
    // Bind(wxEVT_CLOSE_WINDOW, &Help_frame::on_close, this);
}

void Help_frame::on_close(wxCloseEvent&) {
    Hide();
}
