#pragma once

#include <vector>
#include <cmath>

#include <wx/log.h>

#include "task.h"

#include "uni_measurement_manager/event_dispatcher.h"
#include "uni_measurement_manager/measurement_device.h"

// todo refactor

namespace Uni_measurement_manager { // todo add Measurement_task namespace

    template<typename E, typename OP>
        class Operation_handler {
            private:
                E& executor_;
                Measurement_device* measurement_device_;
                Event_dispatcher* event_dispatcher_;
            protected:
                OP operation_;
            public:
                Operation_handler(E& executor, Measurement_device* measurement_device, Event_dispatcher* event_dispatcher)
                    :executor_(executor), measurement_device_(measurement_device), event_dispatcher_(event_dispatcher)
                {}
                OP& operation() { return operation_; }
                void perform() {
                    bool failed = false;
                    try {
                        operation_(measurement_device_);
                    } catch (const Measurement_device_exception& e) {
                        failed = true;
                    }
                    while (failed)
                        try {
                            if (!executor_.is_terminated_or_stopped()) {
                                measurement_device_->connect();
                                operation_(measurement_device_);
                            }
                            failed = false;
                        } catch (const Measurement_device_exception& e) {
                            event_dispatcher_->send_device_error_message(e.what());
                            executor_.pause();
                            executor_.wait_while_paused();
                        }
                }
                void do_perform();
        };

    struct Set_position_operation {
        double position_;
        void operator()(Measurement_device* measurement_device) {
            for (Measurement_device::Drive::iterator_type it = measurement_device->drives_iterator(); it->has_next(); it->next())
                it->get()->move(position_);
        }
    };

    template<typename E>
        class Set_position_operation_handler : public Operation_handler<E, Set_position_operation> {
            public:
                Set_position_operation_handler(E& executor, Measurement_device* measurement_device, Event_dispatcher* event_dispatcher)
                    : Operation_handler<E, Set_position_operation>(executor, measurement_device, event_dispatcher)
                {}
                void set_position(double position) {
                    Operation_handler<E, Set_position_operation>::operation_.position_ = position;
                    Operation_handler<E, Set_position_operation>::perform();
                }
        };

    struct Read_out_operation {
        double read_out_;
        void operator()(Measurement_device* measurement_device) {
            read_out_ = measurement_device->read_out();
        }
    };

    template<typename E>
        class Read_out_operation_handler : public Operation_handler<E, Read_out_operation> {
            public:
                Read_out_operation_handler(E& executor, Measurement_device* measurement_device, Event_dispatcher* event_dispatcher)
                    : Operation_handler<E, Read_out_operation>(executor, measurement_device, event_dispatcher)
                {}
                double read_out() {
                    Operation_handler<E, Read_out_operation>::perform();
                    return Operation_handler<E, Read_out_operation>::operation_.read_out_;
                }
        };

    struct Measurement_parameters {
        double begin_position_;
        double end_position_;
        double step_;
        int averaging_time_;
        int min_samples_;
        int max_samples_;
        double error_;
    };

    class Averager {
        private:
            int min_samples_;
            int max_samples_;
            double error_;
            std::vector<double> samples_;
            double sum_;
        public:
            Averager(int min_samples, int max_samples, double error);
            void average(double value, double& average, double& error, bool& last);
            void clear();
    };

    template<typename E>
        class Measurement_task : public Task<E> {
            private:
                Event_dispatcher* event_dispatcher_;
                const Measurement_parameters parameters_;
            public:
                Measurement_task(Event_dispatcher* event_dispatcher, const Measurement_parameters& parameters)
                    : event_dispatcher_(event_dispatcher), parameters_(parameters)
                {}
                void operator()(E& executor, typename E::facilities_type& facilities) {

                    Measurement_device* measurement_device = facilities.measurement_device();
                    Set_position_operation_handler<E> set_position_handler(executor, measurement_device, event_dispatcher_);
                    Read_out_operation_handler<E> read_out_operation_handler(executor, measurement_device, event_dispatcher_);

                    Averager averager(parameters_.min_samples_, parameters_.max_samples_, parameters_.error_);

                    int count = (parameters_.end_position_ - parameters_.begin_position_) / parameters_.step_;
                    bool stopped = false;

                    for (int i = 0; i <= count && !stopped; ++i) {
                        double x = parameters_.begin_position_ + i * parameters_.step_;

                        set_position_handler.set_position(x);
                        wxLogMessage("position = %e", x);
                        averager.clear();

                        bool last_readout = false;
                        double y_av = 0;
                        double error = -1;
                        do {
                            executor.wait_while_paused();
                            if (executor.is_terminated_or_stopped())
                                stopped = true;

                            if (stopped)
                                last_readout = true;
                            else {
                                executor.sleep(parameters_.averaging_time_);
                                double y = read_out_operation_handler.read_out();
                                wxLogMessage("readout = %e", y);
                                averager.average(y, y_av, error, last_readout);
                                Measurement_payload payload(x, y, y_av, error, last_readout);
                                event_dispatcher_->send_readout_message(payload);
                            }
                        } while (!last_readout);
                    }
                    event_dispatcher_->send_measurement_completed_message();
                }
                void on_pause() {
                    event_dispatcher_->send_paused_message();
                }
        };

}
