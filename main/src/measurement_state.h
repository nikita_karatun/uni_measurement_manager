#pragma once

namespace Uni_measurement_manager {

    class Measurement_context_impl;
    class Measurement_state_controller;
    class Main_frame_impl;

    class Measurement_state {
        public:
            virtual void on_state_set() = 0;
            virtual void switch_position() {}
            virtual void step_position(bool increment) {}
            virtual void toggle_measurement() {}
            virtual void cancel_measurement() {}
            virtual void on_readout_message(const Measurement_payload&) {}

            virtual ~Measurement_state() {};
        protected:
            Measurement_state(Measurement_context_impl* measurement_context, Measurement_state_controller* state_controller) 
                :measurement_context_(measurement_context), state_controller_(state_controller)
            {}
            Measurement_context_impl* const measurement_context_;
            Measurement_state_controller* const state_controller_;
            void do_cancel_measurement();
    };

    /// Measurement stopped, set when corresponding notification comes from measurement executor, also initial state.
    class Stopped_measurement_state : public Measurement_state {
        public:
            Stopped_measurement_state(Measurement_context_impl* measurement_context, Measurement_state_controller* state_controller)
                :Measurement_state(measurement_context, state_controller) 
            {}
            void on_state_set();
            void switch_position();
            void step_position(bool increment);
            void toggle_measurement();
    };

    /// Measurement in progress.
    class Running_measurement_state : public Measurement_state {
        public:
            Running_measurement_state(Measurement_context_impl* measurement_context, Measurement_state_controller* state_controller) 
                :Measurement_state(measurement_context, state_controller)
            {}
            void on_state_set();
            void toggle_measurement();
            void cancel_measurement();
            void on_readout_message(const Measurement_payload&);
    };

    /// Measurement paused, set when corresponding notification comes from measurement executor.
    class Paused_measurement_state : public Measurement_state {
        public:
            Paused_measurement_state(Measurement_context_impl* measurement_context, Measurement_state_controller* state_controller)
                :Measurement_state(measurement_context, state_controller) 
            {}
            void on_state_set();
            void toggle_measurement();
            void cancel_measurement();
    };

    // Pending state, set on the app start and when drive move request is sent, ignores all requests.
    class Pending_measurement_state : public Measurement_state {
        public:
            Pending_measurement_state(Measurement_context_impl* measurement_context, Measurement_state_controller* state_controller)
                :Measurement_state(measurement_context, state_controller) 
            {}
            void on_state_set();
    };

    class Measurement_state_controller {
        public:
            Measurement_state_controller(Measurement_context_impl* measurement_context)
                :stopped_measurement_state_(measurement_context, this),
                running_measurement_state_(measurement_context, this),
                paused_measurement_state_(measurement_context, this),
                pending_measurement_state_(measurement_context, this),
                current_state_(&pending_measurement_state_)
        {}

            Measurement_state* operator->() { return current_state_; }
            Measurement_state* current() { return current_state_; }
            void set_current(Measurement_state* state) {
                current_state_ = state;
                current_state_->on_state_set();
            }

            Measurement_state* stopped() { return &stopped_measurement_state_; }
            Measurement_state* running() { return &running_measurement_state_; }
            Measurement_state* paused() { return &paused_measurement_state_; }
            Measurement_state* pending() { return &pending_measurement_state_; }
        private:
            Stopped_measurement_state stopped_measurement_state_;
            Running_measurement_state running_measurement_state_;
            Paused_measurement_state paused_measurement_state_;
            Pending_measurement_state pending_measurement_state_;
            Measurement_state* current_state_;
    };

}
