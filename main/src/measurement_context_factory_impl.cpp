#include "uni_measurement_manager/application.h"
#include "uni_measurement_manager/measurement_context_factory_impl.h"
#include "uni_measurement_manager/properties_provider_impl.h"
#include "uni_measurement_manager/hot_keys_handler_impl.h"

#include "measurement_settings_impl.h"
#include "measurement_context_impl.h"
#include "event_dispatcher_impl.h"
#include "measurement_executor_impl.h"
#include "measurement_data_impl.h"
#include "readouts_writer_impl.h"
#include "ui_actors_factory_impl.h"

namespace Uni_measurement_manager {

    Measurement_context_factory_impl::Measurement_context_factory_impl() {
    }

    Measurement_context_factory_impl::~Measurement_context_factory_impl() {
    }

    std::auto_ptr<Measurement_context> Measurement_context_factory_impl::create_measurement_context(Application* application) {
        return std::auto_ptr<Measurement_context>(new Measurement_context_impl(application, this));
    }

    std::auto_ptr<Event_dispatcher> Measurement_context_factory_impl::create_event_dispatcher(Measurement_context* measurement_context) {
        return std::auto_ptr<Event_dispatcher>(new Event_dispatcher_impl(measurement_context));
    }

    std::auto_ptr<Measurement_executor> Measurement_context_factory_impl::create_measurement_executor(
            Event_dispatcher* event_dispatcher, Measurement_device_factory* measurement_device_factory) {
        return std::auto_ptr<Measurement_executor>(new Measurement_executor_impl(event_dispatcher, measurement_device_factory));
    }

    std::auto_ptr<Measurement_settings> Measurement_context_factory_impl::create_measurement_settings() {
        std::auto_ptr<Database> database (new Database("settings.db", false));
        std::auto_ptr<Settings_repository> settings_repository(new Settings_repository(*database.get()));
        std::auto_ptr<Measurement_settings> settings(new Measurement_settings_impl(database, settings_repository));
        settings->load();
        return settings;
    }

    std::auto_ptr<Measurement_data> Measurement_context_factory_impl::create_measurement_data(
            Measurement_context* measurement_context, Readouts_writer* readouts_writer) {
        return std::auto_ptr<Measurement_data>(new Measurement_data_impl(measurement_context, readouts_writer));
    }

    std::auto_ptr<Hot_keys_handler> Measurement_context_factory_impl::create_hot_keys_handler(Measurement_context* measurement_context) {
        return std::auto_ptr<Hot_keys_handler>(new Hot_keys_handler_impl(measurement_context));
    }

    std::auto_ptr<Properties_provider> Measurement_context_factory_impl::create_properties_provider() {
        return std::auto_ptr<Properties_provider>(new Properties_provider_impl);
    }

    std::auto_ptr<Readouts_writer> Measurement_context_factory_impl::create_readouts_writer(Properties_provider* properties_provider) {
        return std::auto_ptr<Readouts_writer>(new Readouts_writer_impl(properties_provider));
    }

    std::auto_ptr<Ui_actors_factory> Measurement_context_factory_impl::create_ui_actors_factory(Measurement_context* measurement_context) {
        return std::auto_ptr<Ui_actors_factory>(new Ui_actors_factory_impl(measurement_context));
    }

}
