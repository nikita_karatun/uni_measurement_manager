#include "measurement_settings_impl.h"

namespace Uni_measurement_manager {

    Measurement_settings_impl::Measurement_settings_impl(std::auto_ptr<Database> database,
            std::auto_ptr<Settings_repository> settings_repository)
        :database_(database), settings_repository_(settings_repository) 
    {}

    Measurement_settings_impl::~Measurement_settings_impl() {
        for (Drive::iterator_type it = drives_iterator(); it->has_next(); it->next())
            delete it->get();
    }

    Measurement_settings::Drive* Measurement_settings_impl::get_or_create_drive_by_id(int id) {
        std::map<int, Drive*>::iterator it = drives_.find(id);
        Drive* drive;
        if (it == drives_.end()) {
            drive = create_drive(id);
            drive->set_default_values();
        } else
            drive = it->second;
        return drive;
    }

    Measurement_settings::Drive* Measurement_settings_impl::create_drive(int id) {
        Drive* drive = new Measurement_settings_impl::Drive_impl(id);
        drives_[id] = (drive);
        return drive;
    }

    Measurement_settings::Drive::iterator_type Measurement_settings_impl::drives_iterator() {
        return Drive::iterator_type(new Iterator_impl<std::map<int, Drive*> >(drives_));
    }

    void Measurement_settings_impl::set_default_values() {
        step_ = 0.01;
        averaging_time_ = 300;
        min_samples_ = 5;
        max_samples_ = 30;
        error_ = 0.1;

        for (Drive::iterator_type it = drives_iterator(); it->has_next(); it->next())
            it->get()->set_default_values();
        // for (std::map<int, Measurement_settings::Drive*>::iterator it = drives_.begin(); it != drives_.end(); ++it)
        //     it->second->set_default_values();
    }

    void Measurement_settings_impl::load() {
        settings_repository_->load(this);
    }

    void Measurement_settings_impl::save() {
        settings_repository_->save(this);
    }

    void Measurement_settings_impl::Drive_impl::set_default_values() {
        begin_position_ = 1.0;
        end_position_ = 2.0;
        step_ = 0.01;
    }

}
