#include "wx_custom.h"

#include "uni_measurement_manager/measurement_executor.h"
#include "uni_measurement_manager/positioning_panel.h"
#include "uni_measurement_manager/measurement_settings.h"
#include "uni_measurement_manager/event_payloads.h"
#include "uni_measurement_manager/panels.h"

#include "measurement_state.h"
#include "measurement_context_impl.h"

namespace Uni_measurement_manager {

    void Measurement_state::do_cancel_measurement() {
        wxMessageDialog dialog(measurement_context_->main_frame_, "Are you sure you want to cancel this measurement?",
                "Measurement cancellation", wxYES_NO);
        if (dialog.ShowModal() == wxID_YES) {
            state_controller_->set_current(state_controller_->pending());
            measurement_context_->measurement_executor_->stop();
            measurement_context_->measurement_data_->clear();
            measurement_context_->plot_panel_->set_default_axes();
            measurement_context_->plot_panel_->clear();
            measurement_context_->settings_panel_->SetFocus();
        }
    }

    void Stopped_measurement_state::on_state_set() {
        measurement_context_->buttons_panel_->on_stop();
    }

    void Stopped_measurement_state::switch_position() {
        measurement_context_->positioning_panel_->switch_position();
    }

    void Stopped_measurement_state::step_position(bool increment) {
        measurement_context_->positioning_panel_->step_position(increment);
    }

    void Stopped_measurement_state::toggle_measurement() {
        state_controller_->set_current(state_controller_->running());
        measurement_context_->update_settings_from_ui();

        Measurement_settings::Drive::iterator_type it = measurement_context_->settings()->drives_iterator();
        Measurement_settings::Drive* drive = it->get();

        double begin = drive->begin_position();
        double end = drive->end_position();

        if (begin == end) {
            wxMessageBox("Begin and end match.", wxMessageBoxCaptionStr, wxOK | wxCENTRE | wxICON_WARNING);
            return;
        } 
        if (begin > end) {
            wxMessageBox("Begin point is greater then end.", wxMessageBoxCaptionStr, wxOK | wxCENTRE | wxICON_WARNING);
            return;
        } 
        if (measurement_context_->settings()->step() <= 0) {
            wxMessageBox("Step should be positive non-zero value.", wxMessageBoxCaptionStr, wxOK | wxCENTRE | wxICON_WARNING);
            return;
        }

        if (!measurement_context_->measurement_data_->is_saved()) {
            wxMessageDialog unsaved_measurement_dialog(measurement_context_->main_frame_,
                    "Previous measurement data was not saved and will be lost, would you like to save the data?",
                    "Unsaved measurement", wxYES_NO | wxCANCEL);
            unsaved_measurement_dialog.SetYesNoLabels("Save", "Discard");
            switch (unsaved_measurement_dialog.ShowModal()) {
                case wxID_YES:
                    measurement_context_->save_readouts();
                    break;
                case wxID_CANCEL:
                    return;
            }
        }

       measurement_context_->hide_controls();
       measurement_context_->measurement_data_->clear();
       measurement_context_->plot_panel_->clear();
       measurement_context_->status_bar_->set_default_status_text("In progress");
       measurement_context_->plot_panel_->set_x_limits(begin, end);
       measurement_context_->measurement_executor_->start_measurement(measurement_context_->settings());
    }

    void Running_measurement_state::on_state_set() {
        measurement_context_->buttons_panel_->on_start();
    }

    void Running_measurement_state::toggle_measurement() {
        measurement_context_->hide_controls();
        measurement_context_->buttons_panel_->on_pending();
        measurement_context_->measurement_executor_->pause();
    }

    void Running_measurement_state::cancel_measurement() {
        do_cancel_measurement();
    }

    void Running_measurement_state::on_readout_message(const Measurement_payload& p) {
        measurement_context_->output_panel_->add_sample(p);
        if (p.last_) {
            measurement_context_->measurement_data_->push_back(p);
            measurement_context_->plot_panel_->clear(Plot_panel::Data_set::CURRENT);
            measurement_context_->plot_panel_->add_point(p.x_, p.y_av_, Plot_panel::Data_set::MAIN);
        } else
            measurement_context_->plot_panel_->add_point(p.x_, p.y_, Plot_panel::Data_set::CURRENT);
        measurement_context_->plot_panel_->Refresh();
    }

    void Pending_measurement_state::on_state_set() {
        measurement_context_->buttons_panel_->on_pending();
    }

    void Paused_measurement_state::toggle_measurement() {
        measurement_context_->buttons_panel_->on_start();
        state_controller_->set_current(state_controller_->running());
        measurement_context_->measurement_executor_->resume();
        measurement_context_->status_bar_->set_default_status_text("In progress");
    }

    void Paused_measurement_state::cancel_measurement() {
        do_cancel_measurement();
    }

    void Paused_measurement_state::on_state_set() {
        measurement_context_->buttons_panel_->on_pause();
    }

}
