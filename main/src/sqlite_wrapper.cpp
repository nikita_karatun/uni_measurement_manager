#include "stdafx.h"
#include "sqlite_wrapper.h"

#include <sstream>
#include <cstring>
#include <cstdlib>

#include <stdio.h>

namespace Sqlite_wrapper {

    std::map<int, std::string> compose_error_message_map() {
        std::map<int, std::string> error_message_map_;
        error_message_map_[0] = "SQLITE_OK[0]: Successful result";
        error_message_map_[1] = "SQLITE_ERROR[1]: Generic error";
        error_message_map_[2] = "SQLITE_INTERNAL[2]: Internal logic error in SQLite";
        error_message_map_[3] = "SQLITE_PERM[3]: Access permission denied";
        error_message_map_[4] = "SQLITE_ABORT[4]: Callback routine requested an abort";
        error_message_map_[5] = "SQLITE_BUSY[5]: The database file is locked";
        error_message_map_[6] = "SQLITE_LOCKED[6]: A table in the database is locked";
        error_message_map_[7] = "SQLITE_NOMEM[7]: A malloc() failed";
        error_message_map_[8] = "SQLITE_READONLY[8]: Attempt to write a readonly database";
        error_message_map_[9] = "SQLITE_INTERRUPT[9]: Operation terminated by sqlite3_interrupt()";
        error_message_map_[10] = "SQLITE_IOERR[10]: Some kind of disk I/O error occurred";
        error_message_map_[11] = "SQLITE_CORRUPT[11]: The database disk image is malformed";
        error_message_map_[12] = "SQLITE_NOTFOUND[12]: Unknown opcode in sqlite3_file_control()";
        error_message_map_[13] = "SQLITE_FULL[13]: Insertion failed because database is full";
        error_message_map_[14] = "SQLITE_CANTOPEN[14]: Unable to open the database file";
        error_message_map_[15] = "SQLITE_PROTOCOL[15]: Database lock protocol error";
        error_message_map_[16] = "SQLITE_EMPTY[16]: Internal use only";
        error_message_map_[17] = "SQLITE_SCHEMA[17]: The database schema changed";
        error_message_map_[18] = "SQLITE_TOOBIG[18]: String or BLOB exceeds size limit";
        error_message_map_[19] = "SQLITE_CONSTRAINT[19]: Abort due to constraint violation";
        error_message_map_[20] = "SQLITE_MISMATCH[20]: Data type mismatch";
        error_message_map_[21] = "SQLITE_MISUSE[21]: Library used incorrectly";
        error_message_map_[22] = "SQLITE_NOLFS[22]: Uses OS features not supported on host";
        error_message_map_[23] = "SQLITE_AUTH[23]: Authorization denied";
        error_message_map_[24] = "SQLITE_FORMAT[24]: Not used";
        error_message_map_[25] = "SQLITE_RANGE[25]: 2nd parameter to sqlite3_bind out of range";
        error_message_map_[26] = "SQLITE_NOTADB[26]: File opened that is not a database file";
        error_message_map_[27] = "SQLITE_NOTICE[27]: Notifications from sqlite3_log()";
        error_message_map_[28] = "SQLITE_WARNING[28]: Warnings from sqlite3_log()";
        error_message_map_[100] = "SQLITE_ROW[100]: sqlite3_step() has another row ready";
        error_message_map_[101] = "SQLITE_DONE[101]: sqlite3_step() has finished executing";
        return error_message_map_;
    }

    std::map<int, std::string> error_message_map = compose_error_message_map();

    void Result_set::set_position(int position) {
        position_ = position;
    }

    template<>
        int Result_set::get_next() {
            return sqlite3_column_int(statement_, position_++);
        }

    template<>
        double Result_set::get_next() {
            return sqlite3_column_double(statement_, position_++);
        }

    template<>
        std::string Result_set::get_next() {
            const char* text = (const char*) sqlite3_column_text(statement_, position_++);
            return std::string(text);
        }

    Statement::Statement(sqlite3* db, const char* query) :position_(1) {
        int rc = sqlite3_prepare_v2(db, query, -1, &statement_, NULL);
        if (rc != SQLITE_OK) {
            std::ostringstream ss;
            ss << "failed to create statement. query: '" << query << ", error code: " << rc;
            throw Statement_exception(ss.str());
        }
    }

    Statement::~Statement() {
        sqlite3_finalize(statement_);
    }

    template<>
        Statement& Statement::bind_next(int value) {
            sqlite3_bind_int(statement_, position_++, value);
            return *this;
        }

    template<>
        Statement& Statement::bind_next(double value) {
            sqlite3_bind_double(statement_, position_++, value);
            return *this;
        }

    template<>
        Statement& Statement::bind_next(const char* value) {
            sqlite3_bind_text(statement_, position_++, value, -1, NULL);
            return *this;
        }

    template<>
        Statement& Statement::bind_next(std::string value) {
            return bind_next(value.c_str());
        }

    void Statement::execute() {
        int rc = sqlite3_step(statement_);
        rc_check(rc);
        reset();
    }

    void Statement::rc_check(int rc) {
        if (rc != SQLITE_DONE && rc != SQLITE_ROW) {
            std::ostringstream ss;
            std::map<int, std::string>::iterator it = error_message_map.find(rc);
            ss << "failed to execute statement, error: ";
            if (it != error_message_map.end()) {
                ss << it->second;
            } else {
                ss << rc;
            }
            throw Statement_exception(ss.str());
        }
    }

    void Statement::reset() {
        sqlite3_reset(statement_);
        position_ = 1;
    }

    void error_log_callback(void *pArg, int iErrCode, const char *zMsg){
        fprintf(stderr, "(%d) %s\n", iErrCode, zMsg);
    }

    void print_statements(void* foo, const char* statement) {
        fprintf(stdout, "%s\n", statement);
    }

    Database::Database(const char* filepath, bool trace) {
        sqlite3_config(SQLITE_CONFIG_SINGLETHREAD);
        sqlite3_config(SQLITE_CONFIG_LOG, error_log_callback, NULL);
        if (sqlite3_open(filepath, &db_) != SQLITE_OK)
            throw Connection_exception(std::string("unable to connect to ").append(filepath));
        if (trace)
            sqlite3_trace(db_, print_statements, NULL);

        prepare_statement("PRAGMA foreign_keys").execute(); 
    }

    Database::~Database() {
        sqlite3_close(db_);
    }

    Statement Database::prepare_statement(const char* query) { 
        return Statement(db_, query);
    }
}
