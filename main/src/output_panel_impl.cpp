#include "stdafx.h"
#include "output_panel_impl.h"

#include "wx_custom.h"

#include "uni_measurement_manager/measurement_device.h"
#include "uni_measurement_manager/measurement_settings.h"
#include "uni_measurement_manager/event_payloads.h"
#include "uni_measurement_manager/properties_provider.h"

namespace Uni_measurement_manager {

    const wxString SAMPLE_VALUE_FORMAT = "%.6e";
    const wxString SAMPLE_FORMAT = wxString(SAMPLE_VALUE_FORMAT) << "   %.4e";

    // trailing spaces to make N/A readout same length as valid one to get consistent output panel width
    const wxString SAMPLE_NA_ERROR_FORMAT = wxString(SAMPLE_VALUE_FORMAT) << "   N/A       "; 

    const wxString INITIAL_CURRENT_TEXT = wxString::Format(SAMPLE_FORMAT, 0.0, 0.0);

    Output_panel_impl::Output_panel_impl(wxWindow* parent, Measurement_context* measurement_context) 
        :Output_panel(parent, measurement_context),
        begin_position_field_(NULL), 
        end_position_field_(NULL), 
        step_field_(NULL), 
        averaging_time_field_(NULL), 
        min_samples_field_(NULL), 
        max_samples_field_(NULL), 
        error_field_(NULL), 

        current_text_ctrl_(NULL), 
        log_text_ctrl_(NULL)
        {
            int border_size = measurement_context->properties_provider()->get_int("window.formatting.border_size");
            wxStaticBoxSizer* summary_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this, "Summary");
            {
                wxFlexGridSizer* grid_sizer = new wxFlexGridSizer(2, 2, 8);

                Fields_composer fc(this, grid_sizer);

                begin_position_field_ = fc.add_field("Begin");
                end_position_field_ = fc.add_field("End");
                step_field_ = fc.add_field("Step");
                averaging_time_field_ = fc.add_field("Av. time");
                min_samples_field_ = fc.add_field("Min samples");
                max_samples_field_ = fc.add_field("Max samples");
                error_field_ = fc.add_field("Error");

                grid_sizer->AddGrowableCol(1, 1);

                summary_sizer->Add(grid_sizer, 1, wxALL, border_size);
            }

            wxPanel* log_panel = new wxPanel(this, wxID_ANY);
            {
                log_panel->SetBackgroundColour(*wxBLACK);
                log_panel->SetFont(wxFont(wxNORMAL_FONT->GetPointSize(), wxFONTFAMILY_TELETYPE, wxFONTSTYLE_NORMAL, wxFONTWEIGHT_BOLD));

                wxStaticText* header_text_ctrl = new wxStaticText(log_panel, wxID_ANY, "Value          Error");
                header_text_ctrl->SetForegroundColour(wxColour("#22BC36"));

                current_text_ctrl_ = new wxStaticText(log_panel, wxID_ANY, INITIAL_CURRENT_TEXT);
                current_text_ctrl_->SetForegroundColour(wxColour("#22BC36"));

                log_text_ctrl_ = new wxStaticText(log_panel, wxID_ANY, INITIAL_CURRENT_TEXT);
                log_text_ctrl_->SetForegroundColour(wxColour("#C5C8C6"));

                wxBoxSizer* log_sizer = new wxBoxSizer(wxVERTICAL);
                log_sizer->Add(header_text_ctrl, 0, wxEXPAND | wxLEFT | wxRIGHT | wxTOP, 4);
                log_sizer->Add(current_text_ctrl_, 0, wxEXPAND | wxLEFT | wxRIGHT, 4);
                log_sizer->Add(log_text_ctrl_, 1, wxEXPAND | wxLEFT | wxRIGHT | wxBOTTOM, 4);

                log_panel->SetSizer(log_sizer);
            }

            wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
            sizer->Add(summary_sizer, 0, wxEXPAND);
            sizer->Add(log_panel, 1, wxEXPAND | wxTOP | wxBOTTOM, border_size);
            SetSizer(sizer);
        }

    void Output_panel_impl::render_settings() {
        Measurement_settings::Drive::iterator_type it = settings()->drives_iterator();
        Measurement_settings::Drive* drive = it->get();
        begin_position_field_->set_value(drive->begin_position());
        end_position_field_->set_value(drive->end_position());
        step_field_->set_value(settings()->step());
        averaging_time_field_->set_value(settings()->averaging_time());
        min_samples_field_->set_value(settings()->min_samples());
        max_samples_field_->set_value(settings()->max_samples());
        error_field_->set_value(settings()->error());
    }

    wxString render_sample(double value, double error) {
        return error >= 0
            ? wxString::Format(SAMPLE_FORMAT, value, error)
            : wxString::Format(SAMPLE_NA_ERROR_FORMAT, value);
    }

    void Output_panel_impl::add_sample(const Measurement_payload& p) {
        current_text_ctrl_->SetLabel(render_sample(p.y_av_, p.error_));
        log_ = render_sample(p.y_, p.error_) + "\n" + log_;
        log_text_ctrl_->SetLabel(log_);
        if (p.last_)
            log_.Empty();
    }

    void Output_panel_impl::clear() {
        current_text_ctrl_->SetLabel(INITIAL_CURRENT_TEXT);
        log_text_ctrl_->SetLabel(INITIAL_CURRENT_TEXT);
        log_.Empty();
    }

    Output_panel_impl::Custom_static_text::Custom_static_text(wxWindow* parent) :wxStaticText(parent, wxID_ANY, wxEmptyString) {}

    void Output_panel_impl::Custom_static_text::set_value(int value) {
        SetLabel(Wx_custom::String::to_string(value));
    }

    void Output_panel_impl::Custom_static_text::set_value(double value) {
        SetLabel(wxString::Format("%.3g", value)); // todo parameterize format?
    }

    Output_panel_impl::Fields_composer::Fields_composer(wxWindow* window, wxSizer* sizer) :window_(window), sizer_(sizer) {}

    Output_panel_impl::Custom_static_text* Output_panel_impl::Fields_composer::add_field(wxString label) {
        Output_panel_impl::Custom_static_text* text = new Output_panel_impl::Custom_static_text(window_);
        sizer_->Add(new wxStaticText(window_, wxID_ANY, label), 0);
        sizer_->Add(text, 1);
        return text;
    }

}
