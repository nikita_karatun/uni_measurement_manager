#pragma once

#include "uni_measurement_manager/readouts_writer.h"

#include <memory>

namespace Uni_measurement_manager {

    class Properties_provider;

    class Readouts_writer_impl : public Readouts_writer {
        public:
            Readouts_writer_impl(Properties_provider* properties_provider, size_t file_name_buffer_size = 50);
            std::string compose_file_name();
            std::string file_mask() { return file_mask_; }
            void write(std::vector<Measurement_payload>&, std::ostream&);
        protected:
            const std::string& delimiter() { return delimiter_; }
        private:
            const std::string file_extension_;
            const std::string file_mask_;
            const std::string delimiter_;
            const std::string x_header_;
            const std::string y_header_;
            const std::string error_header_;

            template<typename T>
                void append(std::ostream&, T value, bool add_delimiter = true);
            const size_t file_name_buffer_size_;
            std::auto_ptr<char> file_name_buffer_;
    };

}

