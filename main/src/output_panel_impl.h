#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/panels.h"

namespace Uni_measurement_manager {

    class Measurement_settings;

    class Output_panel_impl : public Output_panel {
        public:
            Output_panel_impl(wxWindow* parent, Measurement_context*);
            void render_settings();
            void add_sample(const Measurement_payload&);
            void clear();
        private:
            class Custom_static_text : public wxStaticText {
                public:
                    Custom_static_text(wxWindow* parent);
                    void set_value(int);
                    void set_value(double);
            };
            class Fields_composer {
                private:
                    wxWindow* window_;
                    wxSizer* sizer_;
                public:
                    Fields_composer(wxWindow* window, wxSizer* sizer);
                    Custom_static_text* add_field(wxString label);
            };

            Custom_static_text* begin_position_field_;
            Custom_static_text* end_position_field_;
            Custom_static_text* step_field_;
            Custom_static_text* averaging_time_field_;
            Custom_static_text* min_samples_field_;
            Custom_static_text* max_samples_field_;
            Custom_static_text* error_field_;

            wxStaticText* current_text_ctrl_;
            wxStaticText* log_text_ctrl_;

            wxString log_;

            Measurement_settings* settings() { return measurement_context_->settings(); }
    };

}
