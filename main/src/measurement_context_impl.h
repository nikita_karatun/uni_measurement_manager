#pragma once

#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/ui_actors_factory.h"
#include "uni_measurement_manager/measurement_device.h"
#include "uni_measurement_manager/properties_provider.h"
#include "uni_measurement_manager/event_dispatcher.h"
#include "measurement_state.h"

namespace Uni_measurement_manager {

    class Measurement_context_impl : public Measurement_context {
        public:
            Measurement_context_impl(Application*, Measurement_context_factory*);
            void init();

            Measurement_settings* settings() { return measurement_settings_.get(); }
            Properties_provider* properties_provider() { return properties_provider_.get(); }

            void layout();
            void enable_save_menu_item(bool enable);
            void hide_controls();
            void update_settings_from_ui();

            void move_drive(int drive_id, Position_type::Enum, double position);
            void step_position(bool increment = true);
            void switch_position();

            void toggle_measurement();
            void cancel_measurement();

            void set_status_bar_tooltip(Status_bar_aware_widget* widget, const wxString& tooltip);
        private:
            friend class Measurement_state;
            friend class Stopped_measurement_state;
            friend class Running_measurement_state;
            friend class Paused_measurement_state;
            friend class Pending_measurement_state;

            Application* const application_;
            const std::auto_ptr<Measurement_device_factory> measurement_device_factory_;
            const std::auto_ptr<Event_dispatcher> event_dispatcher_;
            const std::auto_ptr<Measurement_executor> measurement_executor_;
            const std::auto_ptr<Measurement_settings> measurement_settings_;
            const std::auto_ptr<Hot_keys_handler> hot_key_handler_;
            const std::auto_ptr<Properties_provider> properties_provider_;
            const std::auto_ptr<Readouts_writer> readouts_writer_;
            const std::auto_ptr<Measurement_data> measurement_data_;

            Ui_actors ui_actors_;

            Main_frame* const main_frame_;
            Menu_bar* const menu_bar_;
            Status_bar* const status_bar_;
            Plot_panel* const plot_panel_;
            Input_panel* const input_panel_;
            Positioning_panel* const positioning_panel_;
            Settings_panel* const settings_panel_;
            Output_panel* const output_panel_;
            Buttons_panel* const buttons_panel_;

            Measurement_state_controller state_controller_;

            void save_readouts();

            void on_device_info_message(const Measurement_device_info_payload&);
            void on_readout_message(const Measurement_payload&);
            void on_drive_moved_message(const Drive_move_payload&);
            void on_measurement_completed_message();
            void on_paused_message();

            void on_any_window_focus_set(wxWindow*);

            void on_save(wxCommandEvent&);
            void on_close(wxCloseEvent&);
    };

}
