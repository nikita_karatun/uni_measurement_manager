#include "readouts_writer_impl.h"

#include <fstream>
#include <ctime>

#include "uni_measurement_manager/event_payloads.h"
#include "uni_measurement_manager/properties_provider.h"

namespace Uni_measurement_manager {

    Readouts_writer_impl::Readouts_writer_impl(Properties_provider* properties_provider, size_t file_name_buffer_size) 
        :file_extension_(properties_provider->get_string("output_file.extension")),
        file_mask_(std::string("*.").append(file_extension_)),
        delimiter_(properties_provider->get_string("output_file.delimiter")),
        x_header_(properties_provider->get_string("output_file.header.x")),
        y_header_(properties_provider->get_string("output_file.header.y")),
        error_header_(properties_provider->get_string("output_file.header.error")),

        file_name_buffer_size_(file_name_buffer_size), file_name_buffer_(new char[file_name_buffer_size_]) 
        {}

    std::string Readouts_writer_impl::compose_file_name() {
        time_t t = std::time(NULL);
        tm* tm = std::localtime(&t);
        std::strftime(file_name_buffer_.get(), file_name_buffer_size_, "%Y_%m_%d_%H_%M_%S.", tm);
        return std::string(file_name_buffer_.get()).append(file_extension_);
    }

    void Readouts_writer_impl::write(std::vector<Measurement_payload>& readouts, std::ostream& stream) {
        append(stream, x_header_);
        append(stream, y_header_);
        append(stream, error_header_, false);
        stream << std::endl;
        for (std::vector<Measurement_payload>::iterator it = readouts.begin(); it != readouts.end(); ++it) {
            append(stream, it->x_);
            append(stream, it->y_av_);
            append(stream, it->error_, false);
            stream << std::endl;
        }
    }

    template<typename T>
        void Readouts_writer_impl::append(std::ostream& stream, T value, bool add_delimiter) {
            stream << value;
            if (add_delimiter) stream << delimiter_;
        }

}
