#include "main_frame_impl.h"

#include "uni_measurement_manager/positioning_panel.h"
#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/properties_provider.h"

#include "wx_custom.h"

namespace Uni_measurement_manager {

    Main_frame_impl::Main_frame_impl(Measurement_context* measurement_context)
        : Main_frame(measurement_context->properties_provider()->get_string("application.title")),
        measurement_context_(measurement_context), help_frame_(NULL) // todo make about frame?
    {
        SetBackgroundColour(wxSystemSettings::GetColour(wxSYS_COLOUR_FRAMEBK));
        SetSize(800, 600);

        help_frame_ = new Help_frame(this);

        Bind(wxEVT_MENU, &Main_frame_impl::on_exit, this, wxID_EXIT);
        Bind(wxEVT_MENU, &Main_frame_impl::on_help, this, wxID_HELP);
    }

    void Main_frame_impl::on_exit(wxCommandEvent&) {
        Close();
    }

    void Main_frame_impl::on_help(wxCommandEvent&) {
        help_frame_->Show();
    }

}
