#pragma once

#include "uni_measurement_manager/measurement_context.h"

#include <vector>

namespace Uni_measurement_manager {

    class Measurement_data_impl : public Measurement_data {
        public:
            Measurement_data_impl(Measurement_context*, Readouts_writer*);
            void push_back(const Measurement_payload& p);
            void clear();
            void save(std::ostream&);
            bool is_saved();
        private:
            Measurement_context* measurement_context_;
            Readouts_writer* readouts_writer_;
            std::vector<Measurement_payload> readouts_;
            bool saved_;
    };

}
