#include "measurement_data_impl.h"

#include <iostream>

#include "uni_measurement_manager/readouts_writer.h"
#include "uni_measurement_manager/event_payloads.h"

namespace Uni_measurement_manager {

    Measurement_data_impl::Measurement_data_impl(Measurement_context* measurement_context, Readouts_writer* readouts_writer) 
        :measurement_context_(measurement_context), readouts_writer_(readouts_writer), readouts_(), saved_(true) {}

    void Measurement_data_impl::push_back(const Measurement_payload& p) {
        readouts_.push_back(p);
        saved_ = false;
        measurement_context_->enable_save_menu_item(true);
    }

    void Measurement_data_impl::clear() {
        measurement_context_->enable_save_menu_item(false);
        saved_ = true;
        readouts_.clear();
    }

    void Measurement_data_impl::save(std::ostream& stream) {
        readouts_writer_->write(readouts_, stream);
        saved_ = true;
    }

    bool Measurement_data_impl::is_saved() { return saved_; }

}
