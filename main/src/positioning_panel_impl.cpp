#include "positioning_panel_impl.h" 

#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/properties_provider.h"

#include "event_dispatcher_impl.h"

#include "wx/spinctrl.h"

namespace Uni_measurement_manager {

    Positioning_panel_impl::Positioning_panel_impl(wxWindow* parent, Measurement_context* measurement_context)
        :Positioning_panel(parent, measurement_context)
    {}

    void Positioning_panel_impl::on_device_info_message(const Measurement_device_info_payload& device_info) {
        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        const std::vector<Measurement_drive_info_payload>& drives = device_info.drives_;

        drive_panels_map_.clear();
        for (std::vector<Measurement_drive_info_payload>::const_iterator it = drives.begin(); it != drives.end(); ++it) {
            Measurement_drive_info_payload drive_info = *it;
            Measurement_settings::Drive* drive = measurement_context_->settings()->get_or_create_drive_by_id(drive_info.id_);
            Drive_positioning_panel* drive_panel = new Drive_positioning_panel(this, measurement_context_, drive, drive_info);
            drive_panel->set_begin_position();
            sizer->Add(drive_panel, 1, wxEXPAND);
            drive_panels_map_[drive_info.id_] = drive_panel;
        }

        wxStaticBoxSizer* static_box_sizer = new wxStaticBoxSizer(wxHORIZONTAL, this, 
                measurement_context_->properties_provider()->get_string("panels.positioning.title"));
        static_box_sizer->Add(sizer, 1, wxALL, measurement_context_->properties_provider()->get_int("window.formatting.border_size"));
        SetSizer(static_box_sizer);
    }

    void Positioning_panel_impl::switch_position() {
        drive_panels_map_[DEFAULT_DRIVE_ID]->switch_position(); // todo hardcoded drive id
    }

    void Positioning_panel_impl::step_position(bool increment) {
        drive_panels_map_[DEFAULT_DRIVE_ID]->step_position(increment); // todo hardcoded drive id
    }

    void Positioning_panel_impl::on_drive_moved_message(const Drive_move_payload& p) {
        std::map<int, Drive_positioning_panel*>::iterator it = drive_panels_map_.find(p.drive_id_);
        if (it != drive_panels_map_.end())
            it->second->on_drive_moved_message(p);
    }

    void Positioning_panel_impl::write_settings() {
        for (std::map<int, Drive_positioning_panel*>::iterator it = drive_panels_map_.begin();
                it != drive_panels_map_.end(); ++it)
            it->second->write_settings();
    }

    Drive_positioning_panel::Drive_positioning_panel(
            Positioning_panel_impl* positioning_panel, Measurement_context* measurement_context,
            Measurement_settings::Drive* const drive, const Measurement_drive_info_payload& drive_info)
        :Abstract_panel(positioning_panel, measurement_context), positioning_panel_(positioning_panel),
        position_switch_panel_(NULL), position_ctrl_(NULL),
        drive_(drive), begin_state_(drive), end_state_(drive), position_state_(&begin_state_)
    {
        position_switch_panel_ = new Position_switch_panel(this, measurement_context);

        double increment = measurement_context->properties_provider()->get_double("window.formatting.double_spinner.increment");
        position_ctrl_ = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, 
                wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS | wxTE_PROCESS_ENTER, 
                drive_info.min_position_, drive_info.max_position_, 0, increment);

        step_ctrl_ = new wxSpinCtrlDouble(this, wxID_ANY, wxEmptyString, 
                wxDefaultPosition, wxDefaultSize, wxSP_ARROW_KEYS, 0,
                drive_info.max_position_ - drive_info.min_position_, drive_->step(), increment);

        Status_bar_aware_widget_impl<wxButton>* decr_step_button = new Status_bar_aware_widget_impl<wxButton>();
        decr_step_button->Create(this, wxID_ANY, "<<", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        decr_step_button->SetCanFocus(false);
        measurement_context->set_status_bar_tooltip(decr_step_button, "Decrease position by step value (Ctrl+Z)");

        Status_bar_aware_widget_impl<wxButton>* incr_step_button = new Status_bar_aware_widget_impl<wxButton>();
        incr_step_button->Create(this, wxID_ANY, ">>", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        incr_step_button->SetCanFocus(false);
        measurement_context->set_status_bar_tooltip(incr_step_button, "Increase position by step value (Ctrl+X)");

        wxBoxSizer* step_sizer = new wxBoxSizer(wxHORIZONTAL);
        step_sizer->Add(decr_step_button, 0);
        step_sizer->Add(step_ctrl_, 1);
        step_sizer->Add(incr_step_button, 0);

        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        sizer->Add(position_switch_panel_, 0, wxEXPAND);
        sizer->Add(position_ctrl_, 0, wxEXPAND);
        sizer->Add(step_sizer, 0, wxEXPAND);
        SetSizer(sizer);

        position_ctrl_->Bind(wxEVT_TEXT_ENTER, &Drive_positioning_panel::on_position_ctrl_text_enter, this);
        position_ctrl_->Bind(wxEVT_KILL_FOCUS, &Drive_positioning_panel::on_position_ctrl_focus_lost, this);
        
        decr_step_button->Bind(wxEVT_BUTTON, &Drive_positioning_panel::on_decr_step_button_clicked, this);
        incr_step_button->Bind(wxEVT_BUTTON, &Drive_positioning_panel::on_incr_step_button_clicked, this);
    }
    
    void Drive_positioning_panel::set_begin_position(bool send_device_request) { 
        position_state_ = &begin_state_;
        move_drive(position_state_->get_position(), send_device_request);
    }

    void Drive_positioning_panel::set_end_position(bool send_device_request) { 
        position_state_ = &end_state_; 
        move_drive(position_state_->get_position(), send_device_request);
    }

    void Drive_positioning_panel::switch_position() {
        if (IsEnabled()) position_switch_panel_->switch_position();
    }

    void Drive_positioning_panel::step_position(bool increment) {
        if (IsEnabled()) {
            double position = increment
                ? position_ctrl_->GetValue() + step_ctrl_->GetValue()
                : position_ctrl_->GetValue() - step_ctrl_->GetValue();
            move_drive(position);
        }
    }

    void Drive_positioning_panel::on_drive_moved_message(const Drive_move_payload& p) {
        Enable();
        Position_state* affected_state;
        if (p.position_type_ == Position_type::BEGIN) affected_state = &begin_state_;
        else affected_state = &end_state_;
        if (p.successful_) {
            affected_state->set_position(p.position_);
            if (return_button_pressed_) {
                // have to set focus back to position ctrl, since it lost it being disabled on submit
                position_ctrl_->SetFocus();
                return_button_pressed_ = false;
            }
        } else {
            if (position_switch_panel_->position_switched())
                // switch back to previous position, but without sending move request to drive, 
                // since it is most probably still unavailable and will fail again
                position_switch_panel_->switch_position(false);
            else 
                // set previous position value, since failed to update drive with new one
                position_ctrl_->SetValue(affected_state->get_position());
        }
        position_switch_panel_->on_drive_moved_message();
    }

    void Drive_positioning_panel::move_drive(double position, bool send_device_request) {
        position_ctrl_->SetValue(position);
        if (send_device_request) 
            // actual message to the drive not send for reverting to previous value after error
            move_drive();
    }

    void Drive_positioning_panel::move_drive() {
        Disable();
        measurement_context_->move_drive(drive_->id(), position_state_->position_type(), position_ctrl_->GetValue());
    }

    void Drive_positioning_panel::on_decr_step_button_clicked(wxCommandEvent&) {
        step_position(false);
    }

    void Drive_positioning_panel::on_incr_step_button_clicked(wxCommandEvent&) {
        step_position(true);
    }

    void Drive_positioning_panel::on_position_ctrl_text_enter(wxCommandEvent&) {
        return_button_pressed_ = true;
        move_drive();
    }

    void Drive_positioning_panel::on_position_ctrl_focus_lost(wxFocusEvent& event) {
        if (!return_button_pressed_)
            position_ctrl_->SetValue(position_state_->get_position());
        event.Skip();
    }

    void Drive_positioning_panel::write_settings() {
        drive_->set_step(step_ctrl_->GetValue());
    }


    Position_switch_panel::Position_switch_panel(Drive_positioning_panel* drive_positioning_panel, Measurement_context* measurement_context)
        :wxPanel(drive_positioning_panel, wxID_ANY), drive_positioning_panel_(drive_positioning_panel)
    {
        begin_radio_ = new Status_bar_aware_widget_impl<wxRadioButton>();
        begin_radio_->Create(this, wxID_ANY, "Begin", wxDefaultPosition, wxDefaultSize, wxRB_GROUP);
        measurement_context->set_status_bar_tooltip(begin_radio_, "Begin point (Ctrl+Shift+S to switch)");
        begin_radio_->SetCanFocus(false);

        end_radio_ = new Status_bar_aware_widget_impl<wxRadioButton>();
        end_radio_->Create(this, wxID_ANY, "End");
        measurement_context->set_status_bar_tooltip(end_radio_, "End point (Ctrl+Shift+S to switch)");
        end_radio_->SetCanFocus(false);

        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        sizer->Add(begin_radio_, 0, wxRIGHT, 20);
        sizer->Add(end_radio_, 0, wxBOTTOM, measurement_context->properties_provider()->get_int("window.formatting.border_size") / 2);
        SetSizer(sizer);
        
        begin_radio_->Bind(wxEVT_RADIOBUTTON, &Position_switch_panel::on_begin_switched, this);
        end_radio_->Bind(wxEVT_RADIOBUTTON, &Position_switch_panel::on_end_switched, this);
    }

    void Position_switch_panel::on_begin_switched(wxCommandEvent& event) {
        drive_positioning_panel_->set_begin_position();
        position_switched_ = true;
    }

    void Position_switch_panel::on_end_switched(wxCommandEvent& event) {
        drive_positioning_panel_->set_end_position();
        position_switched_ = true;
    }

    void Position_switch_panel::switch_position(bool send_device_request) {
        position_switched_ = true;
        if (begin_radio_->GetValue()) {
            end_radio_->SetValue(true);
            drive_positioning_panel_->set_end_position(send_device_request);
        } else {
            begin_radio_->SetValue(true);
            drive_positioning_panel_->set_begin_position(send_device_request);
        }
    }

    void Position_switch_panel::on_drive_moved_message() {
        position_switched_ = false;
    }

}
