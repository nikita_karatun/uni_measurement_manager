#include "event_dispatcher_impl.h"

#include "uni_measurement_manager/measurement_context.h"

wxDEFINE_EVENT(mEVT_MEASUREMENT_DEVICE_INFO, wxThreadEvent);
wxDEFINE_EVENT(mEVT_MEASUREMENT_READOUT, wxThreadEvent);
wxDEFINE_EVENT(mEVT_MEASUREMENT_DRIVE_MOVED, wxThreadEvent);
wxDEFINE_EVENT(mEVT_MEASUREMENT_COMPLETED, wxThreadEvent);
wxDEFINE_EVENT(mEVT_MEASUREMENT_PAUSED, wxThreadEvent);
wxDEFINE_EVENT(mEVT_MEASUREMENT_DEVICE_ERROR, wxThreadEvent);

namespace Uni_measurement_manager {

    Event_dispatcher_impl::Event_dispatcher_impl(Measurement_context* measurement_context) 
        :measurement_context_(measurement_context) 
    {
        event_handler_.Bind(mEVT_MEASUREMENT_DEVICE_INFO, &Event_dispatcher_impl::on_device_info_message, this);
        event_handler_.Bind(mEVT_MEASUREMENT_READOUT, &Event_dispatcher_impl::on_readout_message, this);
        event_handler_.Bind(mEVT_MEASUREMENT_DRIVE_MOVED, &Event_dispatcher_impl::on_drive_moved_message, this);
        event_handler_.Bind(mEVT_MEASUREMENT_COMPLETED, &Event_dispatcher_impl::on_measurement_completed_message, this);
        event_handler_.Bind(mEVT_MEASUREMENT_PAUSED, &Event_dispatcher_impl::on_paused_message, this);
        event_handler_.Bind(mEVT_MEASUREMENT_DEVICE_ERROR, &Event_dispatcher_impl::on_device_error_message, this);
    }

    void Event_dispatcher_impl::send_device_info_message(const Measurement_device_info_payload& device_info) {
        queue_event(mEVT_MEASUREMENT_DEVICE_INFO, device_info);
    }

    void Event_dispatcher_impl::on_device_info_message(wxThreadEvent& event) {
        Measurement_device_info_payload device_info = event.GetPayload<Measurement_device_info_payload>();
        measurement_context_->on_device_info_message(device_info);
    }

    void Event_dispatcher_impl::send_readout_message(const Measurement_payload& payload) {
        queue_event(mEVT_MEASUREMENT_READOUT, payload);
    }

    void Event_dispatcher_impl::on_readout_message(wxThreadEvent& event) {
        Measurement_payload p = event.GetPayload<Measurement_payload>();
        measurement_context_->on_readout_message(p);
    }

    void Event_dispatcher_impl::send_drive_moved_message(const Drive_move_payload& payload) {
        queue_event(mEVT_MEASUREMENT_DRIVE_MOVED, payload);
    }

    void Event_dispatcher_impl::on_drive_moved_message(wxThreadEvent& event) {
        Drive_move_payload p = event.GetPayload<Drive_move_payload>();
        measurement_context_->on_drive_moved_message(p);
    }

    void Event_dispatcher_impl::send_measurement_completed_message() {
        wxThreadEvent* event = new wxThreadEvent(mEVT_MEASUREMENT_COMPLETED);
        wxQueueEvent(&event_handler_, event);
    }

    void Event_dispatcher_impl::on_measurement_completed_message(wxThreadEvent&) {
        measurement_context_->on_measurement_completed_message();
    }

    void Event_dispatcher_impl::send_paused_message() {
        wxThreadEvent* event = new wxThreadEvent(mEVT_MEASUREMENT_PAUSED);
        wxQueueEvent(&event_handler_, event);
    }

    void Event_dispatcher_impl::on_paused_message(wxThreadEvent& event) {
        measurement_context_->on_paused_message();
    }

    void Event_dispatcher_impl::send_device_error_message(std::string message) {
        wxThreadEvent* event = new wxThreadEvent(mEVT_MEASUREMENT_DEVICE_ERROR);
        event->SetString(message);
        wxQueueEvent(&event_handler_, event);
    }

    void Event_dispatcher_impl::on_device_error_message(wxThreadEvent& event) {
        wxMessageBox(event.GetString(), wxMessageBoxCaptionStr, wxOK | wxCENTRE | wxICON_ERROR);
    }

    template<typename E, typename P>
        void Event_dispatcher_impl::queue_event(E event_id, P payload) {
            wxThreadEvent* event = new wxThreadEvent(event_id);
            event->SetPayload(payload);
            wxQueueEvent(&event_handler_, event);
        }

}
