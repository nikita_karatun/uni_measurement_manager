#include "uni_measurement_manager/measurement_device_impl.h"

namespace Uni_measurement_manager {
    
    Measurement_device_impl::Drive_impl::Drive_impl(int id) :id_(id) {}

    int Measurement_device_impl::Drive_impl::id() { return id_; }

    Measurement_device_impl::~Measurement_device_impl() {
        for (Drive::iterator_type it = drives_iterator(); it->has_next(); it->next())
            delete it->get();
    }

    void Measurement_device_impl::add_drive(Drive* drive) {
        std::auto_ptr<Drive> d(drive);
        int id = d->id();
        if (ids_.find(id) != ids_.end()) {
            std::stringstream ss;
            ss << "id uniqueness violation, id = " << id;
            throw Measurement_device_exception(ss.str());
        }
        drives_.push_back(d.release());
        ids_.insert(id);
    }

    Measurement_device::Drive::iterator_type Measurement_device_impl::drives_iterator() {
        return Drive::iterator_type(new Iterator_impl<std::vector<Drive*> >(drives_));
    }

}
