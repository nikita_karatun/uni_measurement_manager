#include "plot_panel_impl.h"

namespace Uni_measurement_manager {

    Plot_panel_impl::Plot_panel_impl(wxWindow* parent, Measurement_context* measurement_context) :Plot_panel(parent, measurement_context) {
        plot_ = new Spectrometer::SimplePlot(this, wxID_ANY);

        wxBoxSizer* sizer = new wxBoxSizer(wxVERTICAL);
        sizer->Add(plot_, 1, wxEXPAND);
        SetSizer(sizer);

        set_default_axes();
        plot_->create_data_set();
        plot_->create_data_set()
            .set_mark_pen(wxRED_PEN)
            .set_line_pen(NULL)
            .set_pointed_mark_pen(NULL);
    }

    void Plot_panel_impl::set_default_axes() {
        plot_->set_x_limits(0, 10);
        plot_->set_y_limits(0, 1);
    }

    void Plot_panel_impl::clear(Data_set::e data_set) {
        if (data_set == Data_set::ALL) {
            (*plot_)[Data_set::MAIN].clear();
            (*plot_)[Data_set::CURRENT].clear();
        } else
            (*plot_)[data_set].clear();
    }

    void Plot_panel_impl::add_point(double x, double y, Data_set::e data_set) {
        if (data_set == Data_set::ALL) {
            (*plot_)[Data_set::MAIN].add_point(x, y);
            (*plot_)[Data_set::CURRENT].add_point(x, y);
        } else
            (*plot_)[data_set].add_point(x, y);
    }

    void Plot_panel_impl::set_x_limits(double begin, double end) {
        plot_->set_x_limits(begin, end);
    }

}
