#pragma once

#include "database.h"

namespace Uni_measurement_manager {

    class Measurement_settings;
    
    class Settings_repository {
        private:
            Sqlite_wrapper::Statement select_settings_statement_;
            Sqlite_wrapper::Statement insert_settings_statement_;
            Sqlite_wrapper::Statement update_statement_;

            Sqlite_wrapper::Statement select_drives_statement_;
            Sqlite_wrapper::Statement select_drive_ids_statement_;
            Sqlite_wrapper::Statement insert_drive_statement_;
            Sqlite_wrapper::Statement update_drive_statement_;
        public:
            Settings_repository(Database& database);

            void load(Measurement_settings*);
            void save(Measurement_settings*);
    };

}
