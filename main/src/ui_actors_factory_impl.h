#pragma once

#include "uni_measurement_manager/ui_actors_factory.h"

namespace Uni_measurement_manager {

    class Measurement_context;

    class Ui_actors_factory_impl : public Ui_actors_factory {
        public:
            Ui_actors_factory_impl(Measurement_context*);
            Ui_actors create_ui_actors();
            Main_frame* create_main_frame();
            Menu_bar* create_menu_bar();
            Status_bar* create_status_bar(wxFrame* parent);
            Plot_panel* create_plot_panel(wxWindow* parent);
            Input_panel* create_input_panel(wxWindow* parent);
            Positioning_panel* create_positioning_panel(wxWindow* parent);
            Settings_panel* create_settings_panel(wxWindow* parent);
            Output_panel* create_output_panel(wxWindow* parent);
            Buttons_panel* create_buttons_panel(wxWindow* parent);
        private:
            Measurement_context* measurement_context_;
    };

}
