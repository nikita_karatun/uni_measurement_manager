#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/properties_provider.h"

#include "buttons_panel_impl.h"

#include "wx_custom.h"

namespace Uni_measurement_manager {

    Buttons_panel_impl::Buttons_panel_impl(wxWindow* parent, Measurement_context* measurement_context) 
        :Buttons_panel(parent, measurement_context), resume_button_(NULL), cancel_button_(NULL)
    {
        spinner_ = new wxAnimationCtrl(this, wxID_ANY, wxNullAnimation, wxDefaultPosition, wxSize(24, 24));
        spinner_file_loaded_ = spinner_->LoadFile(wxT("resources/rolling-1s-24px.gif"));

        resume_button_ = new Status_bar_aware_widget_impl<wxButton>();
        resume_button_->Create(this, wxID_ANY, "Start", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        resume_button_->SetCanFocus(false);

        cancel_button_ = new Status_bar_aware_widget_impl<wxButton>();
        cancel_button_->Create(this, wxID_ANY, "Cancel", wxDefaultPosition, wxDefaultSize, wxBU_EXACTFIT);
        cancel_button_->SetCanFocus(false);
        on_pending();

        int border_size = measurement_context_->properties_provider()->get_int("window.formatting.border_size");
        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        sizer->Add(spinner_, 0, wxALIGN_CENTER | wxRIGHT | wxLEFT, border_size);
        sizer->Add(resume_button_, 0, wxALIGN_CENTER | wxRIGHT, border_size);
        sizer->Add(cancel_button_, 0, wxALIGN_CENTER);

        SetSizer(sizer);

        resume_button_->Bind(wxEVT_BUTTON, &Buttons_panel_impl::on_resume_button_clicked, this);
        cancel_button_->Bind(wxEVT_BUTTON, &Buttons_panel_impl::on_cancel_button_clicked, this);
    }

    void Buttons_panel_impl::post_construct() {
        measurement_context_->set_status_bar_tooltip(resume_button_, "Start (Ctrl+Shift+B)");
        measurement_context_->set_status_bar_tooltip(cancel_button_, "Cancel (Ctrl+Shift+C)");
    }

    void Buttons_panel_impl::on_start() {
        enable_buttons(true, true);
        resume_button_->SetLabel("Pause");
        resume_button_->set_status_bar_tooltip("Pause (Ctrl+Shift+B)");
        measurement_context_->layout();
    }

    void Buttons_panel_impl::on_pause() {
        enable_buttons(true, true);
        resume_button_->SetLabel("Resume");
        resume_button_->set_status_bar_tooltip("Resume (Ctrl+Shift+B)");
        measurement_context_->layout();
    }

    void Buttons_panel_impl::on_stop() {
        enable_buttons(true, false);
        resume_button_->SetLabel("Start");
        resume_button_->set_status_bar_tooltip("Start (Ctrl+Shift+B)");
        measurement_context_->layout();
    }

    void Buttons_panel_impl::on_pending() {
        enable_buttons(false, false);
        if (spinner_file_loaded_) {
            spinner_->Show();
            GetParent()->Layout();
            spinner_->Play();
        } else
            Layout();
    }

    void Buttons_panel_impl::enable_buttons(bool resume_button, bool cancel_button) {
        resume_button_->Enable(resume_button);
        cancel_button_->Enable(cancel_button);
        if (resume_button || cancel_button)
            hide_spinner();
    }

    void Buttons_panel_impl::hide_spinner() {
        if (spinner_file_loaded_) {
            spinner_->Stop();
            spinner_->Hide();
        }
    }

    void Buttons_panel_impl::on_resume_button_clicked(wxCommandEvent&) {
        measurement_context_->toggle_measurement();
    }

    void Buttons_panel_impl::on_cancel_button_clicked(wxCommandEvent&) {
        measurement_context_->cancel_measurement();
    }

}
