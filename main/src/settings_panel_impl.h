#pragma once

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/spinctrl.h>

#include "uni_measurement_manager/panels.h"
#include "uni_measurement_manager/measurement_context.h"
#include "settings_repository.h"

namespace Uni_measurement_manager {

    class Settings_panel_impl : public Settings_panel {
        public:
            Settings_panel_impl(wxWindow* parent, Measurement_context*);
            void on_device_info_message(const Measurement_device_info_payload&);
            void write_settings();
        private:
            wxSpinCtrlDouble* step_field_;
            wxSpinCtrl* averaging_time_field_;
            wxSpinCtrl* min_samples_field_;
            wxSpinCtrl* max_samples_field_;
            wxSpinCtrlDouble* error_field_;
    };

}
