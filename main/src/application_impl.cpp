#include "stdafx.h"
#include "uni_measurement_manager/application_impl.h"

#include <clocale>

#ifdef WIN32
// to set exponent length (_set_output_format) on windows (3 digits by default)
#include "stdio.h"
// have to assign the result to variable, otherwise following function invocation
// is parsed as function declaration
int old_length = _set_output_format(_TWO_DIGIT_EXPONENT);
#endif

#include "uni_measurement_manager/measurement_context_factory_impl.h"

namespace Uni_measurement_manager {

    bool Application_impl::OnInit() {
        std::setlocale(LC_ALL, "en_US.UTF-8");
        measurement_context_ = create_measurement_context_factory()->create_measurement_context(this);
        measurement_context_->init();
        return true;
    }

    int Application_impl::OnExit() {
        return 0;
    }

    std::auto_ptr<Measurement_context_factory> Application_impl::create_measurement_context_factory() {
        return std::auto_ptr<Measurement_context_factory>(new Measurement_context_factory_impl());
    }

}

