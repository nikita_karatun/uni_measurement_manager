#pragma once

#include <iostream>

#include "wx/thread.h"

#include "task.h"

namespace Uni_measurement_manager {

    template<typename FC>
        class Single_thread_executor {
            public:
                typedef typename FC::facilities_type facilities_type;
                typedef Task<Single_thread_executor<FC> > task_type;

                Single_thread_executor(FC, bool auto_started = true);

                void start();
                template<typename T>
                    void execute(T task);
                void pause(); 
                void resume(); 
                void wait_while_paused();
                void stop();
                bool is_stopped();

                void terminate(); 
                bool is_terminated(); 

                bool is_terminated_or_stopped();

                void join(); 

                void sleep(long ms); 
            private:
                struct Ex_thread : public wxThread {
                    Single_thread_executor& executor_;
                    Ex_thread(Single_thread_executor&);
                    ExitCode Entry();
                };

                FC facilities_composer_;
                const bool auto_started_;

                wxMutex mutex_;
                wxCondition condition_variable_;
                task_type* task_;
                bool terminated_;
                bool paused_;
                bool stopped_;

                Ex_thread thread_;

                void run_thread();
        };

}

#include "task_executor_impl.h"

