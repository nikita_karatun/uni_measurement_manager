#pragma once

#include "uni_measurement_manager/measurement_settings.h"

#include <memory>
#include <map>

#include "database.h"
#include "settings_repository.h"

// todo support const objects?

namespace Uni_measurement_manager {

    class Measurement_settings_impl : public Measurement_settings {
        public:
            Measurement_settings_impl(std::auto_ptr<Database>, std::auto_ptr<Settings_repository>);
            ~Measurement_settings_impl();

            Measurement_settings::Drive* get_or_create_drive_by_id(int id);

            int id() { return id_; }
            double step() { return step_; }
            int averaging_time() { return averaging_time_; }
            int min_samples() { return min_samples_; }
            int max_samples() { return max_samples_; }
            double error() { return error_; }

            void set_id(int id) { id_ = id; }
            void set_step(double step) { step_ = step; }
            void set_averaging_time(int averaging_time){ averaging_time_ = averaging_time; }
            void set_min_samples(int min_samples){ min_samples_ = min_samples; }
            void set_max_samples(int max_samples){ max_samples_ = max_samples; }
            void set_error(double error){ error_ = error; }

            Drive* create_drive(int id);
            Drive::iterator_type drives_iterator();

            void set_default_values();

            void load();
            void save();
        private:
            class Drive_impl : public Measurement_settings::Drive {
                public:
                    Drive_impl(int id) :id_(id) {}
                    Drive_impl(
                            int id,
                            double begin_position,
                            double end_position,
                            double step) 
                        :
                            id_(id), 
                            begin_position_(begin_position),
                            end_position_(end_position),
                            step_(step) {}

                    int id() { return id_; }
                    double begin_position(){ return begin_position_; }
                    double end_position(){ return end_position_; }
                    double step(){ return step_; }

                    void set_begin_position(double begin_position){ begin_position_ = begin_position; }
                    void set_end_value(double end_position){ end_position_ = end_position; }
                    void set_step(double step){ step_ = step; }

                    void set_default_values();
                private:
                    const int id_; 
                    double begin_position_;
                    double end_position_;
                    double step_;
                    bool auto_update_;

            };
            std::auto_ptr<Database> database_;
            std::auto_ptr<Settings_repository> settings_repository_;

            int id_;
            double step_;
            int averaging_time_;
            int min_samples_;
            int max_samples_;
            double error_;

            std::map<int, Measurement_settings::Drive*> drives_;
    };

}
