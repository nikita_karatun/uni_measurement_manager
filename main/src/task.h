#pragma once

namespace Uni_measurement_manager {

    template <typename E>
        struct Task {
            virtual void operator()(E& ex, typename E::facilities_type& facilities) = 0;
            virtual void on_pause() {}
            virtual ~Task() {}
        };

}
