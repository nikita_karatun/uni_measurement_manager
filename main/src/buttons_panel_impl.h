#pragma once

#include "uni_measurement_manager/panels.h"

#include "status_bar_impl.h"

#include "wx/animate.h"

namespace Uni_measurement_manager {

    class Buttons_panel_impl : public Buttons_panel {
        public:
            Buttons_panel_impl(wxWindow* parent, Measurement_context* measurement_context);
            void post_construct();
            void on_start();
            void on_pause();
            void on_stop();
            void on_pending();
        private:
            Status_bar_aware_widget_impl<wxButton>* resume_button_;
            Status_bar_aware_widget_impl<wxButton>* cancel_button_;

            wxAnimationCtrl* spinner_;
            bool spinner_file_loaded_;

            void enable_buttons(bool resume_button, bool cancel_button);
            void hide_spinner();

            void on_resume_button_clicked(wxCommandEvent&);
            void on_cancel_button_clicked(wxCommandEvent&);
    };

}
