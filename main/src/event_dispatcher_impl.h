#pragma once

#include "uni_measurement_manager/event_dispatcher.h"
#include "uni_measurement_manager/event_payloads.h"

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

namespace Uni_measurement_manager {

    class Measurement_context;

    class Event_dispatcher_impl : public Event_dispatcher {
        private:
            Measurement_context* const measurement_context_;
            wxEvtHandler event_handler_;
        public:
            Event_dispatcher_impl(Measurement_context* measurement_context_);

            void send_device_info_message(const Measurement_device_info_payload&);
            void send_readout_message(const Measurement_payload&);
            void send_drive_moved_message(const Drive_move_payload&);
            void send_measurement_completed_message();
            void send_paused_message();
            void send_device_error_message(std::string message);
        private:
            void on_device_info_message(wxThreadEvent&);
            void on_readout_message(wxThreadEvent&);
            void on_drive_moved_message(wxThreadEvent&);
            void on_measurement_completed_message(wxThreadEvent&);
            void on_paused_message(wxThreadEvent&);
            void on_device_error_message(wxThreadEvent&);

            template<typename E, typename P>
                void queue_event(E event, P payload);
    };

}
