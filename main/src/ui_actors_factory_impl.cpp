#include "ui_actors_factory_impl.h"

#include "uni_measurement_manager/measurement_context.h"
#include "uni_measurement_manager/properties_provider.h"

#include "measurement_context_impl.h"

#include "main_frame_impl.h"
#include "menu_bar_impl.h"
#include "plot_panel_impl.h"
#include "positioning_panel_impl.h"
#include "settings_panel_impl.h"
#include "output_panel_impl.h"
#include "buttons_panel_impl.h"

namespace Uni_measurement_manager {

    Ui_actors_factory_impl::Ui_actors_factory_impl(Measurement_context* measurement_context)
        :measurement_context_(measurement_context)
    {}

    Ui_actors Ui_actors_factory_impl::create_ui_actors() {
        Main_frame* main_frame = create_main_frame();

        Menu_bar* menu_bar = create_menu_bar();
        Status_bar* status_bar = create_status_bar(main_frame);
        main_frame->SetMenuBar(menu_bar);
        main_frame->SetStatusBar(status_bar);

        Plot_panel* plot_panel = create_plot_panel(main_frame);
        Input_panel* input_panel = create_input_panel(main_frame);
        Positioning_panel* positioning_panel = create_positioning_panel(input_panel);
        Settings_panel* settings_panel = create_settings_panel(input_panel);

        int border_size = measurement_context_->properties_provider()->get_int("window.formatting.border_size");
        wxBoxSizer* input_panel_sizer = new wxBoxSizer(wxVERTICAL);
        input_panel_sizer->Add(positioning_panel, 0, wxEXPAND | wxUP | wxRIGHT | wxLEFT, border_size);
        input_panel_sizer->Add(settings_panel, 0, wxEXPAND | wxUP | wxRIGHT | wxLEFT, border_size);
        input_panel->SetSizer(input_panel_sizer);

        Output_panel* output_panel = create_output_panel(main_frame);
        output_panel->Hide();
        Buttons_panel* buttons_panel = create_buttons_panel(main_frame);

        wxBoxSizer* right_sizer = new wxBoxSizer(wxVERTICAL);
        right_sizer->Add(input_panel, 1);
        right_sizer->Add(output_panel, 1, wxEXPAND | wxUP | wxRIGHT | wxLEFT, border_size);
        right_sizer->Add(buttons_panel, 0, wxALIGN_RIGHT | wxRIGHT, border_size);

        wxBoxSizer* sizer = new wxBoxSizer(wxHORIZONTAL);
        sizer->Add(plot_panel, 1, wxEXPAND);
        sizer->Add(right_sizer, 0, wxEXPAND);

        main_frame->SetSizer(sizer);
        main_frame->Layout();

        return Ui_actors(main_frame, menu_bar, status_bar, plot_panel, input_panel, positioning_panel,
                settings_panel, output_panel, buttons_panel);
    }

    Main_frame* Ui_actors_factory_impl::create_main_frame() {
        return new Main_frame_impl(measurement_context_);
    }

    Menu_bar* Ui_actors_factory_impl::create_menu_bar() {
        return new Uni_measurement_manager::Menu_bar_impl;
    }

    Status_bar* Ui_actors_factory_impl::create_status_bar(wxFrame* parent) {
        return new Status_bar_impl(parent);
    }

    Plot_panel* Ui_actors_factory_impl::create_plot_panel(wxWindow* parent) {
        return new Plot_panel_impl(parent, measurement_context_);
    }

    Input_panel* Ui_actors_factory_impl::create_input_panel(wxWindow* parent) {
        return new Input_panel(parent, measurement_context_);
    }

    Positioning_panel* Ui_actors_factory_impl::create_positioning_panel(wxWindow* parent) {
        return new Positioning_panel_impl(parent, measurement_context_);
    }

    Settings_panel* Ui_actors_factory_impl::create_settings_panel(wxWindow* parent) {
        return new Settings_panel_impl(parent, measurement_context_);
    }

    Output_panel* Ui_actors_factory_impl::create_output_panel(wxWindow* parent) {
        return new Output_panel_impl(parent, measurement_context_);
    }

    Buttons_panel* Ui_actors_factory_impl::create_buttons_panel(wxWindow* parent) {
        return new Buttons_panel_impl(parent, measurement_context_);
    }

}
