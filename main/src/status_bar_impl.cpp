#include "status_bar_impl.h"

namespace Uni_measurement_manager {

    Status_bar_impl::Status_bar_impl(wxWindow* parent, long style, const wxString& name)
        :Status_bar(parent, style, name)
    {}

    void Status_bar_impl::set_default_status_text(const wxString& text, int i) {
        default_values_[i] = text;
        SetStatusText(text, i);
    }

    void Status_bar_impl::set_default_status_text(int i) {
        SetStatusText(default_values_[i]);
    }

}
