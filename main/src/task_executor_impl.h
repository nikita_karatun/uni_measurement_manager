#pragma once

namespace Uni_measurement_manager {

    template<typename FC>
        Single_thread_executor<FC>::Ex_thread::Ex_thread(Single_thread_executor& ex) :wxThread(wxTHREAD_JOINABLE), executor_(ex) {}

    template<typename FC>
        wxThread::ExitCode Single_thread_executor<FC>::Ex_thread::Entry() {
            executor_.run_thread();
            return (wxThread::ExitCode) 0;
        }

    template<typename FC>
        Single_thread_executor<FC>::Single_thread_executor(FC facilities_composer, bool auto_started) 
        :facilities_composer_(facilities_composer), auto_started_(auto_started),
        condition_variable_(mutex_), task_(NULL), terminated_(false), paused_(false), thread_(*this) {
            if (auto_started)
                thread_.Run();
        }

    template<typename FC>
        void Single_thread_executor<FC>::start() {
            thread_.Run();
        }

    template<typename FC>
        template<typename T>
        void Single_thread_executor<FC>::execute(T task) {
            wxMutexLocker locker(mutex_);
            if (!task_) {
                task_ = new T(task);
                paused_ = false;
                stopped_ = false;
                condition_variable_.Signal(); 
            }
        }

    template<typename FC>
        void Single_thread_executor<FC>::pause() {
            wxMutexLocker locker(mutex_);
            paused_ = true;
        }

    template<typename FC>
        void Single_thread_executor<FC>::resume() {
            wxMutexLocker locker(mutex_);
            paused_ = false;
            condition_variable_.Signal(); 
        }

    template<typename FC>
        void Single_thread_executor<FC>::wait_while_paused() {
            wxMutexLocker locker(mutex_);
            bool first = true;
            while (paused_) {
                if (first) {
                    if (task_)
                        task_->on_pause();
                    first = false;
                }
                condition_variable_.Wait();
            }
        }

    template<typename FC>
        void Single_thread_executor<FC>::stop() {
            wxMutexLocker locker(mutex_);
            paused_ = false;
            stopped_ = true;
            condition_variable_.Signal(); 
        }

    template<typename FC>
        bool Single_thread_executor<FC>::is_stopped() {
            wxMutexLocker locker(mutex_);
            return stopped_;
        }

    template<typename FC>
        void Single_thread_executor<FC>::terminate() {
            wxMutexLocker locker(mutex_);
            paused_ = false;
            terminated_ = true;
            condition_variable_.Signal(); 
        }

    template<typename FC>
        bool Single_thread_executor<FC>::is_terminated() {
            wxMutexLocker locker(mutex_);
            return terminated_;
        }

    template<typename FC>
        bool Single_thread_executor<FC>::is_terminated_or_stopped() {
            wxMutexLocker locker(mutex_);
            return terminated_ || stopped_;
        }

    template<typename FC>
        void Single_thread_executor<FC>::join() {
            thread_.Wait();
        }

    template<typename FC>
        void Single_thread_executor<FC>::sleep(long ms) {
            wxThread::This()->Sleep(ms);
        }

    template<typename FC>
        void Single_thread_executor<FC>::run_thread() {
            typename FC::facilities_type facilities = facilities_composer_.compose();
            while (!is_terminated()) {
                {
                    wxMutexLocker locker(mutex_);
                    while (!task_ && !terminated_) condition_variable_.Wait();
                }
                if (!is_terminated()) {
                    (*task_)(*this, facilities);
                    wxMutexLocker locker(mutex_);
                    delete task_;
                    task_ = NULL;
                }
            }
        }

}
