#pragma once

#include "sqlite_wrapper.h"

namespace Uni_measurement_manager {

    class Database : public Sqlite_wrapper::Database {
        public:
            Database(const char* filepath, bool trace = false);
    };

}
