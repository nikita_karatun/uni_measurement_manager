#pragma once

#include "uni_measurement_manager/panels.h"

namespace Uni_measurement_manager {

    class Menu_bar_impl : public Menu_bar {
        public:
            Menu_bar_impl();
            void enable_save_item(bool enable);
        private:
            wxMenuItem* save_menu_item_;
    };

}
