#pragma once

#include <map>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <wx/valnum.h>

#include "uni_measurement_manager/common.h"

namespace Wx_custom {

    namespace String {
        inline wxString to_string(int i) {
            return wxString::Format("%d", i);
        }
    }

}

