#include "menu_bar_impl.h"

namespace Uni_measurement_manager {

    Menu_bar_impl::Menu_bar_impl() {
        wxMenu* file_menu = new wxMenu();

        save_menu_item_ = file_menu->Append(wxID_SAVE, wxEmptyString, "Save current results");
        save_menu_item_->Enable(false);

        file_menu->AppendSeparator();
        file_menu->Append(wxID_EXIT);

        wxMenu* help_menu = new wxMenu();
        help_menu->Append(wxID_ABOUT); // todo add about

        this->Append(file_menu, "&File");
        this->Append(help_menu, "&Help");
    }

    void Menu_bar_impl::enable_save_item(bool enable) {
        save_menu_item_->Enable(enable);
    }

}
