#pragma once

#include "uni_measurement_manager/positioning_panel.h"
#include "uni_measurement_manager/measurement_settings.h"

#include "wx_custom.h"

#include "status_bar_impl.h"

class wxSpinCtrlDouble;

namespace Uni_measurement_manager {

    class Drive_positioning_panel;

    class Positioning_panel_impl : public Positioning_panel {
        public:
            Positioning_panel_impl(wxWindow* parent, Measurement_context*);
            void switch_position();
            void step_position(bool increment);
            void on_device_info_message(const Measurement_device_info_payload&);
            void on_drive_moved_message(const Drive_move_payload&);
            void write_settings();
        private:
            std::map<int, Drive_positioning_panel*> drive_panels_map_;
    };

    class Position_state {
        public:
            Position_state(Position_type::Enum position_type, Measurement_settings::Drive* const drive) 
                :position_type_(position_type), drive_(drive) 
            {}
            virtual ~Position_state() {}
            virtual double get_position() = 0;
            virtual void set_position(double position) = 0;
            Position_type::Enum position_type() { return position_type_; }
        protected:
            Position_type::Enum position_type_;
            Measurement_settings::Drive* const drive_; 
    };

    class Begin_state : public Position_state {
        public:
            Begin_state(Measurement_settings::Drive* const drive) :Position_state(Position_type::BEGIN, drive) {}
            double get_position() { return drive_->begin_position(); }
            void set_position(double position) { return drive_->set_begin_position(position); }
    };

    class End_state : public Position_state {
        public:
            End_state(Measurement_settings::Drive* const drive) :Position_state(Position_type::END, drive) {}
            double get_position() { return drive_->end_position(); }
            void set_position(double position) { return drive_->set_end_value(position); }
    };

    class Measurement_drive_info_payload;
    class Position_switch_panel;

    class Drive_positioning_panel : public Abstract_panel {
        public:
            Drive_positioning_panel(Positioning_panel_impl*, Measurement_context*, Measurement_settings::Drive* const, const Measurement_drive_info_payload&);
            void set_begin_position(bool send_device_request = true);
            void set_end_position(bool send_device_request = true);
            void switch_position();
            void step_position(bool increment);
            void move_drive(double position, bool send_device_request = true);
            void move_drive();
            void on_drive_moved_message(const Drive_move_payload&);
            void write_settings();
        private:
            Positioning_panel_impl* positioning_panel_;
            Position_switch_panel* position_switch_panel_;
            wxSpinCtrlDouble* position_ctrl_;
            wxSpinCtrlDouble* step_ctrl_;
            Measurement_settings::Drive* const drive_; 
            Begin_state begin_state_;
            End_state end_state_;
            Position_state* position_state_;

            bool return_button_pressed_; // rename

            void on_decr_step_button_clicked(wxCommandEvent&);
            void on_incr_step_button_clicked(wxCommandEvent&);
            void on_position_ctrl_text_enter(wxCommandEvent&);
            void on_position_ctrl_focus_lost(wxFocusEvent&);
    };

    class Position_switch_panel : public wxPanel {
        public:
            Position_switch_panel(Drive_positioning_panel* drive_positioning_panel, Measurement_context*);
            void switch_position(bool send_device_request = true);
            bool position_switched() { return position_switched_; }
            void on_drive_moved_message();
        private:
            Status_bar_aware_widget_impl<wxRadioButton>* begin_radio_;
            Status_bar_aware_widget_impl<wxRadioButton>* end_radio_;
            Drive_positioning_panel* drive_positioning_panel_;

            bool position_switched_;

            void on_begin_switched(wxCommandEvent& event);
            void on_end_switched(wxCommandEvent& event);
    };

}
