#pragma once

#include <stdexcept>
#include <string>
#include <map>
#include <vector>
#include <set>

#include <sqlite3.h>

namespace Sqlite_wrapper {

    class Database_exception : public std::runtime_error {
        public:
            Database_exception(std::string what) :std::runtime_error(what) {}
    };

    class Connection_exception : public Database_exception {
        public:
            Connection_exception(std::string what) :Database_exception(what) {}
    };

    class Statement_exception : public Database_exception {
        public:
            Statement_exception(std::string what) :Database_exception(what) {}
    };

    class Result_set {
        private:
            sqlite3_stmt* statement_;
            int position_;
        public:
            Result_set(sqlite3_stmt* statement) :statement_(statement), position_(0) {}
            void set_position(int position);

            template<typename T>
                T get_next();
    };

    class Statement {
        private:
            sqlite3_stmt* statement_;
            int position_;

            void rc_check(int rc);
            void reset();
        public:
            Statement(sqlite3* db, const char* query);
            ~Statement();

            template<typename T>
                Statement& bind_next(T value);

            void execute();

            template<typename M>
                void query(M& mapper);
            template<typename T, typename M>
                T query_one(M mapper);
            template<typename T>
                T query_to_container();
            template<typename T, typename M>
                T query_to_container(M& mapper);
    };

    class Database {
        public:
            sqlite3* db_;
            Database(const char* filepath, bool trace = false);
            ~Database();

            Statement prepare_statement(const char* query);
    };

    template<typename T, typename M>
        T Statement::query_one(M mapper) {
            int rc = sqlite3_step(statement_);
            rc_check(rc);
            Result_set result_set(statement_);
            T t = mapper(result_set);
            reset();
            return t;
        }

    template<typename M>
        void Statement::query(M& mapper) {
            int rc;
            do {
                rc = sqlite3_step(statement_);
                if (rc == SQLITE_ROW) {
                    Result_set result_set(statement_);
                    mapper(result_set);
                }
            } while (rc == SQLITE_ROW);
            rc_check(rc);
            reset();
        }

    template<typename C, typename M>
        struct Container_mapper {
            M inner_mapper_;
            C container_;
            Container_mapper(M& inner_mapper) :inner_mapper_(inner_mapper) {}

            template<typename CO, typename E>
                void add_element(CO& container, E element) {
                    container.push_back(element);
                }
            template<typename E>
                void add_element(std::vector<E>& container, E element) {
                    container.push_back(element);
                }
            template<typename E>
                void add_element(std::set<E>& container, E element) {
                    container.insert(element);
                }

            void operator()(Result_set& result_set) {
                add_element<typename C::value_type>(container_, inner_mapper_(result_set));
            }
        };

    template<typename T>
        struct Single_mapper {
            int operator()(Result_set& result_set) {
                return result_set.get_next<T>();
            }
        };

    template<typename T>
        T Statement::query_to_container() {
            typedef typename T::value_type element_type;
            Single_mapper<element_type> mapper;
            Container_mapper<T, Single_mapper<element_type> > vector_mapper(mapper);
            query<Container_mapper<T, Single_mapper<element_type> > > (vector_mapper);
            return vector_mapper.container_;
        }

    template<typename T, typename M>
        T Statement::query_to_container(M& mapper) {
            Container_mapper<T, M> vector_mapper(mapper);
            query<Container_mapper<T, M> >(vector_mapper);
            return vector_mapper.container_;
        }

}
