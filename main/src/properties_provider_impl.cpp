#include "uni_measurement_manager/properties_provider_impl.h"

namespace Uni_measurement_manager {

    Properties_provider_impl::Properties_provider_impl() {
        add_property("application.title", "Uni measurement manager");
        add_property("panels.positioning.title", "Positioning");

        add_property("window.formatting.border_size", 8);
        add_property("window.formatting.double_spinner.increment", 0.001);

        add_property("output_file.extension", "csv");
        add_property("output_file.delimiter", ",");
        add_property("output_file.header.x", "Position");
        add_property("output_file.header.y", "Value");
        add_property("output_file.header.error", "Error");

    }

    std::string Properties_provider_impl::get_string(const std::string& key) {
        return do_get_property<std::string>(strings_, key, "");
    }

    int Properties_provider_impl::get_int(const std::string& key) {
        return do_get_property<int>(ints_, key, 0);
    }

    double Properties_provider_impl::get_double(const std::string& key) {
        return do_get_property<double>(doubles_, key, 0);
    }

    Properties_provider_impl* Properties_provider_impl::add_property(const std::string& key, const std::string& value) {
        strings_[key] = value;
        return this;
    }

    Properties_provider_impl* Properties_provider_impl::add_property(const std::string& key, int value) {
        ints_[key] = value;
        return this;
    }

    Properties_provider_impl* Properties_provider_impl::add_property(const std::string& key, double value) {
        doubles_[key] = value;
        return this;
    }

    template<typename T>
        T Properties_provider_impl::do_get_property(const typename std::map<std::string, T> map, const std::string& key,
                const T default_value) {
            typename std::map<std::string, T>::const_iterator it = map.find(key);
            return it == map.end() ? default_value : it->second;
        }

}
