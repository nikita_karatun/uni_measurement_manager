#pragma once

#include <map>

#include "uni_measurement_manager/measurement_executor.h"
#include "uni_measurement_manager/measurement_device.h"
#include "task_executor.h"

namespace Uni_measurement_manager {

    class Facilities;
    class Event_dispatcher;

    class Facilities_composer {
        public:
            typedef Facilities facilities_type;
            Facilities_composer(Event_dispatcher*, Measurement_device_factory*);
            Facilities compose();
        private:
            Event_dispatcher* const event_dispatcher_;
            Measurement_device_factory* measurement_device_factory_;
    };

    class Facilities {
        public:
            Facilities(Event_dispatcher* event_dispatcher, Measurement_device* measurement_device);
            Facilities(const Facilities&);
            ~Facilities();
            Event_dispatcher* event_dispatcher() { return event_dispatcher_; }
            Measurement_device* measurement_device() { return measurement_device_; }
            Measurement_device::Drive* get_drive_by_id(int id);
        private:
            Event_dispatcher* const event_dispatcher_;
            mutable Measurement_device* measurement_device_;
            std::map<int, Measurement_device::Drive*> drives_;
    };

    class Measurement_executor_impl : public Measurement_executor, private Single_thread_executor<Facilities_composer> {
        public:
            Measurement_executor_impl(Event_dispatcher*, Measurement_device_factory*);

            void start() { Single_thread_executor<Facilities_composer>::start(); }
            void pause() { Single_thread_executor<Facilities_composer>::pause(); } 
            void resume() { Single_thread_executor<Facilities_composer>::resume(); } 
            void stop() { Single_thread_executor<Facilities_composer>::stop(); }
            void terminate() { Single_thread_executor<Facilities_composer>::terminate(); } 
            void join() { Single_thread_executor<Facilities_composer>::join(); }

            void start_measurement(Measurement_settings*);
            void move_drive(int drive_id, Position_type::Enum, double position);
        private:
            Event_dispatcher* const event_dispatcher_;
    };

}
