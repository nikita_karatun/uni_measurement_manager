#include "stdafx.h"
#include "measurement_task.h"

#include <cmath>

namespace Uni_measurement_manager {

    Averager::Averager(int min_samples, int max_samples, double error) 
        :min_samples_(min_samples), max_samples_(max_samples), error_(error), samples_() 
    {}

    void Averager::average(double value, double& average, double& error, bool& last) {
        samples_.push_back(value);
        int size = (int) samples_.size();
        sum_ += value;
        average = sum_ / size;

        if (size > 1) {
            double diff_sum = 0;
            for (std::vector<double>::iterator it = samples_.begin(); it != samples_.end(); ++it) 
                diff_sum += pow(*it - average, 2);
            error = sqrt(diff_sum / (size - 1));
        } else 
            error = -1;

        last = (size >= min_samples_ && error <= error_) || size >= max_samples_;
    }

    void Averager::clear() {
        samples_.clear();
        sum_ = 0;
    }

}
