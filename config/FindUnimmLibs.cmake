# Find Unimm Headers/Libs

# Variables
# UNIMM_ROOT - set this to a location where Unimm may be found
#
# UNIMM_FOUND - True of Unimm found
# UNIMM_INCLUDE_DIRS - Location of Unimm includes
# UNIMM_LIBRARIES - Unimm libraries
# SIMPLE_PLOT_LIBRARIES - Simple plot libraries

include(FindPackageHandleStandardArgs)

if (NOT UNIMM_ROOT)
    set(UNIMM_ROOT "$ENV{UNIMM_ROOT}")
endif()

if (NOT UNIMM_ROOT)
    find_path(_UNIMM_ROOT NAMES include/uni_measurement_manager)
else()
    set(_UNIMM_ROOT "${UNIMM_ROOT}")
endif()

function(__unimm_find_library _name _lib_name)
    if (NOT ${CMAKE_CXX_PLATFORM_ID} STREQUAL "Windows")
        find_library(${_name} NAMES uni_measurement_manager HINTS ${_UNIMM_ROOT})
    else()
        find_library(__release_library NAMES ${_lib_name} HINTS ${_UNIMM_ROOT})
        find_library(__debug_library NAMES ${_lib_name}d HINTS ${_UNIMM_ROOT})
        set(${_name} optimized "${__release_library}" debug "${__debug_library}" PARENT_SCOPE)
    endif()
endfunction()

if (_UNIMM_ROOT)
    set(UNIMM_INCLUDE_DIRS ${_UNIMM_ROOT}/include)
    __unimm_find_library(UNIMM_LIBRARIES uni_measurement_manager)
    __unimm_find_library(SIMPLE_PLOT_LIBRARIES simple_plot)
endif()

find_package_handle_standard_args(UnimmLibs FOUND_VAR UnimmLibs_FOUND
    REQUIRED_VARS UNIMM_INCLUDE_DIRS UNIMM_LIBRARIES)

