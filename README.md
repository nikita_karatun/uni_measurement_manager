# Universal measurement manager

C++ cross-platform framework for simple general purpose measurement applications.

Application built on the framework performs measurement of the dependency of one value on the another.

Examples of such dependencies may be but not limited by following (*please note that the framework is actually used and tested with only first example, other application support may require additional development*).

* The dependency of the signal value on the boxcar gate delay for the emission lifetime measurement.
* The dependency of the signal value on the diffraction grating position for the spectroscopy measurement.
* The dependency of the signal value on the bulk sample position for the absorption or luminescence profile measurement.

The application needs to define measurement device which performs read-outs and contains one or more drives which change depends-on value.

Sample boxcar application can be found in samples directory.

*Although measurement device may contain more the one drive to change depend-on value only one drive currently supported by the framework.*

## Measurement procedure

For the screenshots provided further sample boxcar application used.

Drive initial and final position are set up in Position panel which is renamed to Delay for sample Boxcar application, since it is boxcar delay value which is stepped through for that particular application.

* The measurement performed from begin to end point with step specified in the *Step field*.
* *Av. time* (Averaging time) field sets time to wait before reading out next value and calculating the average and error.
* *Min samples* field defines minimum samples to read out regardless of current error value.
* *Max samples* field defines maximum samples to read out.
* *Error* field defines maximum error allowed.

Read outs performed minimum *Min samples* times, till averaging error less then or equals to *Error* or *Max samples* read outs count reached.

![screenshot_measurement_completed](docs/images/screenshot_measurement_completed.png)

While the measurement in progress, current readouts are reflected in the log panel.

![screenshot_measurement_in_progress](docs/images/screenshot_measurement_in_progress.png)

## Further goals

* Multiple drives support (to move two diffraction gratings for the spectroscopic measurement for instance).

* Multiple drives spectrometer application sample.

## Tested on

* Xubuntu 19.04
* Windows XP

## Build requirements

* [Cmake](https://cmake.org)
* a C++98-standard-compliant compiler
* [wxWidgets](https://www.wxwidgets.org)
* [sqlite3](https://www.sqlite.org)

## For tests

* [googletest](https://github.com/google/googletest)
