#pragma once

#include <map>
#include <vector>
#include <string>
#include <algorithm>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "../src/plot_math.h" // todo
#include "../src/plot_painter.h"

namespace Spectrometer {

    struct Point {
        // original (real) x and y
        double x_;
        double y_;
        // normalized for plot y (log_10(y))
        double norm_y_;
        // x and y in pixels of plot window
        int pix_x_;
        int pix_y_;
    };

    class SimplePlot;

    class Data_set {
        public:
            Data_set(SimplePlot*);
			Data_set& operator=(Data_set& o);
            void add_point(double x, double y, bool update_limits = true);
            void clear();
            Data_set& set_line_pen(const wxPen*);
            Data_set& set_mark_pen(const wxPen*);
            Data_set& set_pointed_mark_pen(const wxPen*);
        private:
            friend class Data_set_painter;
            friend class SimplePlot;
            friend class Plot_sizes;
            friend class Plot_painter;
            SimplePlot* plot_;
            std::vector<Point> points_;
            DLimits_2 limits_;

            const wxPen* line_pen_;
            const wxPen* mark_pen_;
            const wxPen* pointed_mark_pen_;
    };

    struct Plot_sizes {
        const SimplePlot& plot_;

        Axes axes_;
        DLimits_2 points_limits_;

        double x_coef_;
        double y_coef_;

        wxCoord window_width_;
        wxCoord window_height_;
        wxCoord plot_width_;
        wxCoord plot_height_;

        wxCoord window_left_offset_;
        wxCoord window_right_offset_;
        wxCoord offset_;

        wxCoord mouse_x_;
        wxCoord mouse_y_;

        Plot_sizes(const SimplePlot& plot);
        void update();
        void update_limits(DLimits_2 limits, bool compare);
        wxCoord x_to_pixels(double x) const;
        wxCoord y_to_pixels(double y_norm) const;
    };

    class SimplePlot : public wxWindow {
        public:
            SimplePlot(wxWindow *parent, wxWindowID id, const wxPoint &pos=wxDefaultPosition,
                    const wxSize &size=wxDefaultSize, long style=0, const wxString &name=wxPanelNameStr);
            void set_x_limits(double, double);
            void set_y_limits(double, double);
            Data_set& create_data_set();
            Data_set& operator[](size_t index); // todo return data sets vector instead of [] operator?
            void clear();
            bool AcceptsFocusFromKeyboard() const;
        protected:
            void OnPaint(wxPaintEvent&);
            void OnEraseBackground(wxEraseEvent&);
            void OnMouseLeave(wxMouseEvent&);
            void OnMouseMove(wxMouseEvent&);
            void OnSize(wxSizeEvent&);
        private:
            friend class Plot_sizes;
            friend class Data_set;
            Plot_sizes sizes_;
            std::vector<Data_set> data_sets_;
            Plot_painter plot_painter_;

            long points_count_;
            bool has_points();
    };

}
