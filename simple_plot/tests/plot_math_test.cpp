#include "gtest_custom.h"

#include <iostream>
#include <wx/log.h>

#include "plot_math.h"

class Simple_plot_math_test : public ::testing::Test {
    protected:
        static void SetUpTestCase() {
            wxLog* logger = new wxLogStream(&std::cout);
            wxLog::SetActiveTarget(logger);
        }
};

TEST_F(Simple_plot_math_test, test_1) {
    Axis axis(0.015, 0.1);
    ASSERT_DOUBLE_EQ(axis.first_, 0.02);
    ASSERT_DOUBLE_EQ(axis.step_, 0.01);
}

TEST_F(Simple_plot_math_test, test_2) {
    Axis axis(-0.015, 0.1);
    ASSERT_DOUBLE_EQ(axis.first_, 0);
    ASSERT_DOUBLE_EQ(axis.step_, 0.025);
}

TEST_F(Simple_plot_math_test, test_3) {
    Axis axis(0, 1000);
    ASSERT_DOUBLE_EQ(axis.first_, 100);
    ASSERT_DOUBLE_EQ(axis.step_, 100);
}

TEST_F(Simple_plot_math_test, test_4) {
    Axis axis(-2.01061E-06, 2.39423E-05, true);
    ASSERT_DOUBLE_EQ(axis.min_, -2.5E-06);
    ASSERT_DOUBLE_EQ(axis.first_, 0);
    ASSERT_DOUBLE_EQ(axis.step_, 5E-06);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.max_, 2.4E-05);
}

TEST_F(Simple_plot_math_test, test_5) {
    Axis axis(1.494095e-07, 1.568402e-07, true);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.min_, 1.494E-07);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.first_, 1.5E-07);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.step_, 1E-09);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.max_, 1.569E-07);
}

TEST_F(Simple_plot_math_test, test_6) {
    Axis axis(1.52946e-007, 1.53474e-007, true);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.min_, 1.5294e-07);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.first_, 1.53E-07);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.step_, 1E-10);
    ASSERT_DOUBLE_EQ_RELATIVE(axis.max_, 1.5348e-07);
}

TEST_F(Simple_plot_math_test, test_7) {
    Axis axis(1.5298e-007, 1.53471e-007, true);
    // todo add asserts
}
