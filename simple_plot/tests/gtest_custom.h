#pragma once

#include "gtest/gtest.h"

#ifndef RELATIVE_ERROR
#define RELATIVE_ERROR 1E-10
#endif

namespace testing {
    namespace internal {

        template <typename RawType>
            AssertionResult CmpHelperFloatingPointEQRelative(const char* lhs_expression,
                    const char* rhs_expression,
                    RawType lhs_value,
                    RawType rhs_value) {

                if ((std::abs(rhs_value - lhs_value) / rhs_value) < RELATIVE_ERROR)
                    return AssertionSuccess();

                ::std::stringstream lhs_ss;
                lhs_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
                    << lhs_value;

                ::std::stringstream rhs_ss;
                rhs_ss << std::setprecision(std::numeric_limits<RawType>::digits10 + 2)
                    << rhs_value;

                return EqFailure(lhs_expression,
                        rhs_expression,
                        StringStreamToString(&lhs_ss),
                        StringStreamToString(&rhs_ss),
                        false);
            }

    }
}

#define ASSERT_DOUBLE_EQ_RELATIVE(val1, val2)\
    ASSERT_PRED_FORMAT2(testing::internal::CmpHelperFloatingPointEQRelative<double>, \
            val1, val2)
