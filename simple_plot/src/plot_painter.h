#pragma once

#include <vector>
#include <cfloat>

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "plot_math.h"

namespace Spectrometer {

    class Plot_sizes;
    class Data_set;
    class Point;

    class Pointed_point_checker {
        private:
            const wxCoord mouse_x_;
            const wxCoord mouse_y_;
            const wxCoord mouse_x_min_;
            const wxCoord mouse_x_max_;
            const wxCoord mouse_y_norm_min_;
            const wxCoord mouse_y_norm_max_;
            Point* pointed_point_;
        public:
            Pointed_point_checker(wxCoord mouse_x, wxCoord mouse_y);
            bool check(Point* point);
            Point* pointed_point();
    };

    class Data_set_painter {
        public:
            Data_set_painter(const Plot_sizes& sizes);
            void draw(wxDC&, Pointed_point_checker&, Data_set&);
        private:
            const Plot_sizes& sizes_;

            void draw_mark(wxDC&, Point*, Pointed_point_checker&,
                    const wxPen* mark_pen, const wxPen* pointed_mark_pen);
            void draw_point_label(wxDC&, Point*);
    };

    class Plot_painter {
        public:
            Plot_painter(const Plot_sizes& sizes, std::vector<Data_set>& data_sets);
            void paint(wxDC&);
        private:
            const Plot_sizes& sizes_;
            std::vector<Data_set>& data_sets_;
            Data_set_painter data_set_painter_;
    };

}

