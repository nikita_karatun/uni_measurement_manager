#include "stdafx.h"
#include "plot_painter.h"

#include <cmath>

#include "wx_simple_plot.h"

const wxString PLOT_NUMBER_FORMAT = "%.6g";
const wxString PLOT_POINT_LABEL_FORMAT = wxString(PLOT_NUMBER_FORMAT) << "\n" << PLOT_NUMBER_FORMAT;

namespace Spectrometer {

    const int CIRCLE_DOT_SIZE = 4;
    const wxCoord LABEL_BOX_CURSOR_X_OFFSET = 15;
    const wxCoord LABEL_BOX_CURSOR_Y_OFFSET = 23;

    template<typename P>
        class Axis_painter : P {
            public:
                Axis_painter(wxDC& dc, const Plot_sizes& sizes) :P(dc, sizes) {}
                void draw() {
                    Axis axis = P::axis_;
                    draw(axis.min_, false);
                    double first = axis.first_;
                    if (first - axis.min_ < axis.step_ / 5)
                        first += axis.step_;
                    int count = (axis.max_ - axis.step_ / 5 - first) / axis.step_;
                    for (int i = 0; i <= count; ++i)
                        draw(first + i * axis.step_);
                    draw(axis.max_, false);
                }
            private:
                void draw(double coord, bool draw_line = true) {
                    wxCoord pixels = P::to_pixels(coord);
                    if (draw_line)
                        P::draw_axis(pixels);
                    wxString label = wxString::Format(PLOT_NUMBER_FORMAT, coord);
                    wxCoord w;
                    wxCoord h;
                    P::dc_.GetTextExtent(label, &w, &h);
                    P::draw_label(label, pixels, w, h);
                }
        };

    class Base_axis_painter {
        public:
            Base_axis_painter(wxDC& dc, const Plot_sizes& sizes, const Axis& axis) :dc_(dc), sizes_(sizes), axis_(axis) {}
        protected:
            wxDC& dc_;
            const Plot_sizes& sizes_;
            const Axis& axis_;
    };

    class X_axis_painter : public Base_axis_painter {
        public:
            X_axis_painter(wxDC& dc, const Plot_sizes& sizes) :Base_axis_painter(dc, sizes, sizes.axes_.x_) {}
        protected:
            wxCoord to_pixels(double x) {
                return sizes_.x_to_pixels(x);
            }
            void draw_axis(wxCoord x) {
                dc_.DrawLine(x, sizes_.offset_, x, sizes_.plot_height_ + sizes_.offset_);
            }
            void draw_label(wxString label, wxCoord x, wxCoord w, wxCoord h) {
                dc_.DrawText(label, x - w / 2, sizes_.plot_height_ + sizes_.offset_ + 5);
            }
    };

    class Y_axis_painter : public Base_axis_painter {
        public:
            Y_axis_painter(wxDC& dc, const Plot_sizes& sizes) :Base_axis_painter(dc, sizes, sizes.axes_.y_) {}
        protected:
            wxCoord to_pixels(double y) {
                return sizes_.y_to_pixels(y);
            }
            void draw_axis(wxCoord y) {
                dc_.DrawLine(sizes_.window_left_offset_, y, sizes_.window_left_offset_ + sizes_.plot_width_, y);
            }
            void draw_label(wxString label, wxCoord y, wxCoord w, wxCoord h) {
                dc_.DrawText(label, sizes_.window_left_offset_ - w - 5, y - h / 2);
            }
    };

    Pointed_point_checker::Pointed_point_checker(wxCoord mouse_x, wxCoord mouse_y)
        : mouse_x_(mouse_x), mouse_y_(mouse_y), 
        mouse_x_min_(mouse_x - CIRCLE_DOT_SIZE), mouse_x_max_(mouse_x + CIRCLE_DOT_SIZE),
        mouse_y_norm_min_(mouse_y - CIRCLE_DOT_SIZE), mouse_y_norm_max_(mouse_y + CIRCLE_DOT_SIZE),
        pointed_point_ (NULL) 
    {}

    bool Pointed_point_checker::check(Point* point) {
        bool b(false);
        if (mouse_x_ > 0 && !pointed_point_ && point->pix_x_ > mouse_x_min_ && point->pix_x_ < mouse_x_max_
                && point->pix_y_ > mouse_y_norm_min_ && point->pix_y_ < mouse_y_norm_max_) {
            pointed_point_ = point;
            b = true;
        }
        return b;
    }

    Point* Pointed_point_checker::pointed_point() { return pointed_point_; }

    Data_set_painter::Data_set_painter(const Plot_sizes& sizes) :sizes_(sizes) {}

    void Data_set_painter::draw_mark(wxDC& dc, Point* point, Pointed_point_checker& pointed_point_checker,
            const wxPen* mark_pen, const wxPen* pointed_mark_pen) {
        const wxPen* pen = NULL;
        if (pointed_mark_pen && pointed_point_checker.check(point))
            pen = pointed_mark_pen;
        if (!pen && mark_pen)
            pen = mark_pen;
        if (pen) {
            dc.SetPen(*pen);
            dc.DrawCircle(point->pix_x_, point->pix_y_, CIRCLE_DOT_SIZE);
        }
    }

    void Data_set_painter::draw(wxDC& dc, Pointed_point_checker& pointed_point_checker, Data_set& data_set) {
        if (!data_set.points_.empty()) {
            const wxPen* line_pen = data_set.line_pen_;
            const wxPen* mark_pen = data_set.mark_pen_;
            const wxPen* pointed_mark_pen = data_set.pointed_mark_pen_;

            std::vector<Point>::iterator end = data_set.points_.end();
            Point* previous = NULL;
            for (std::vector<Point>::iterator p = data_set.points_.begin(); p != end; ++p) {
                p->pix_x_ = sizes_.x_to_pixels(p->x_); // todo sufficient to calculate on resize
                p->pix_y_ = sizes_.y_to_pixels(p->norm_y_);
                if (previous) {
                    if (line_pen) {
                        dc.SetPen(*line_pen);
                        dc.DrawLine(previous->pix_x_, previous->pix_y_, p->pix_x_, p->pix_y_);
                    }
                    draw_mark(dc, previous, pointed_point_checker, mark_pen, pointed_mark_pen);
                }
                previous = &*p;
            }
            draw_mark(dc, &*(--end), pointed_point_checker, mark_pen, pointed_mark_pen);
            dc.SetPen(*wxBLACK_PEN);
            draw_point_label(dc, pointed_point_checker.pointed_point());
        }
    }

    void Data_set_painter::draw_point_label(wxDC& dc, Point* point) {
        if (point) {
            wxCoord x = sizes_.mouse_x_ + LABEL_BOX_CURSOR_X_OFFSET;
            wxCoord y = sizes_.mouse_y_ + LABEL_BOX_CURSOR_Y_OFFSET;
            wxCoord w;
            wxCoord h;
            wxString label = wxString::Format(PLOT_POINT_LABEL_FORMAT, point->x_, point->y_);
            dc.GetMultiLineTextExtent(label, &w, &h);
            wxCoord text_border_offset = 5;
            wxCoord border_x = x - text_border_offset;
            wxCoord border_y = y - text_border_offset;
            wxCoord border_w = w + text_border_offset * 2;
            wxCoord border_h = h + text_border_offset * 2;
            if (border_x + border_w > sizes_.window_width_) {
                border_x = border_x - border_w - LABEL_BOX_CURSOR_X_OFFSET;
                x = border_x + text_border_offset;
            }
            if (border_y + border_h > sizes_.window_height_) {
                border_y = border_y - border_h - LABEL_BOX_CURSOR_Y_OFFSET;
                y = border_y + text_border_offset;
            }
            dc.DrawRoundedRectangle(border_x, border_y, border_w, border_h, 5);
            dc.DrawText(label, x, y);
        }
    }

    Plot_painter::Plot_painter(const Plot_sizes& sizes, std::vector<Data_set>& data_sets)
        :sizes_(sizes), data_sets_(data_sets), data_set_painter_(sizes)
    {}

    void Plot_painter::paint(wxDC& dc) {
        dc.Clear();
        dc.DrawRectangle(sizes_.window_left_offset_, sizes_.offset_, sizes_.plot_width_, sizes_.plot_height_);

        Axis_painter<X_axis_painter>(dc, sizes_).draw();
        Axis_painter<Y_axis_painter>(dc, sizes_).draw();

        dc.SetPen(*wxRED_PEN);
        if (sizes_.mouse_x_ > 0 
                && sizes_.window_left_offset_ < sizes_.mouse_x_ && sizes_.mouse_x_ < sizes_.plot_width_ + sizes_.window_left_offset_ 
                && sizes_.offset_ < sizes_.mouse_y_ && sizes_.mouse_y_ < sizes_.plot_height_ + sizes_.offset_) {
            dc.DrawLine(sizes_.mouse_x_, sizes_.offset_, sizes_.mouse_x_, sizes_.plot_height_ + sizes_.offset_);
            {
                wxString label = wxString::Format(PLOT_NUMBER_FORMAT,
                        (sizes_.mouse_x_ - sizes_.window_left_offset_) / sizes_.x_coef_ + sizes_.axes_.x_.min_);
                wxCoord w;
                wxCoord h;
                dc.GetTextExtent(label, &w, &h);
                dc.DrawText(label, sizes_.mouse_x_ - w / 2, sizes_.offset_ - h - 4);
            }
            dc.DrawLine(sizes_.window_left_offset_, sizes_.mouse_y_, sizes_.window_left_offset_ + sizes_.plot_width_ , sizes_.mouse_y_);
            {
                wxString label = wxString::Format(PLOT_NUMBER_FORMAT, 
                        sizes_.axes_.y_.min_ + ((double) (sizes_.offset_ + sizes_.plot_height_ - sizes_.mouse_y_)) / sizes_.y_coef_);
                wxCoord w;
                wxCoord h;
                dc.GetTextExtent(label, &w, &h);
                wxCoord y;
                if (sizes_.mouse_y_ - w / 2 < 0)
                    y = w;
                else if (sizes_.mouse_y_ + w / 2 > sizes_.window_height_)
                    y = sizes_.window_height_;
                else
                    y = sizes_.mouse_y_ + w / 2;
                dc.DrawRotatedText(label, sizes_.window_left_offset_ + sizes_.plot_width_ + 4, y, 90.0);
            }
        }

        Pointed_point_checker pointed_point_checker(sizes_.mouse_x_, sizes_.mouse_y_);

        for (std::vector<Data_set>::iterator it = data_sets_.begin(); it != data_sets_.end(); ++it) {
            data_set_painter_.draw(dc, pointed_point_checker, *it);
        }
    }

}
