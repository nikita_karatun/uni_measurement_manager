#include "stdafx.h"
#include "plot_math.h"

#include <iostream>
#include <sstream>
#include <cmath>
#include <stdexcept>
#include <cfloat>

#include <wx/log.h>

inline double round_number(double number) {
    if (std::abs(number) > 0.4) {
        if (number < 0.0)
            number = ceil(number - 0.5);
        else if (number > 0.0)
            number = floor(number + 0.5);
    } else
        number = 0.0;
    return number;
}

// todo remove this class?
Fixed_point_number::Fixed_point_number(double value) :mantissa_(0), exponent_(0) {
    double mantissa;
    double rounded;
    do {
        mantissa = exponent_ == 0 ? value : mantissa * 10;
        rounded = round_number(mantissa); // rint used previously (not found on windows xp)
        --exponent_;	
    } while (std::abs(mantissa - rounded) > DBL_EPSILON);
    ++exponent_;
    mantissa_ = rounded;
}

template<typename T>
std::ostream& number_to_stream(std::ostream& stream, T number) {
    stream << number.mantissa_ << "E" << number.exponent_;
    return stream;
}

template<typename T>
std::string number_to_string(T number) {
    std::stringstream stream;
    number_to_stream(stream, number);
    return stream.str();
}


std::ostream& operator<<(std::ostream& stream, Fixed_point_number number) {
    return number_to_stream(stream, number);
}

void normalize(Fixed_point_number& n, double exponent) {
    int diff = n.exponent_ - exponent;
    if (diff > 0) {
        n.mantissa_ *= pow(10.0, diff);
        n.exponent_ = exponent;
    }
}

void normalize(Fixed_point_number& n1, Fixed_point_number& n2) {
    int exponent = std::min(n1.exponent_, n2.exponent_);
    normalize(n1, exponent);
    normalize(n2, exponent);
}

Floating_point_number::Floating_point_number(double d) :mantissa_(d), exponent_(0) {
    if (mantissa_ != 0) {
        bool positive = true;
        if (mantissa_ < 0) {
            mantissa_ *= -1;
            positive = false;
        }
        while (mantissa_ < 1) {
            mantissa_ *= 10;
            --exponent_;
        }
        while (mantissa_ > 10) {
            mantissa_ /= 10;
            ++exponent_;
        }
        if (!positive)
            mantissa_ *= -1;
    }
}

double Floating_point_number::to_double() {
    return mantissa_ * pow(10.0, exponent_);
}

std::ostream& operator<<(std::ostream& stream, Floating_point_number number) {
    return number_to_stream(stream, number);
}

Axis::Axis(double min, double max, bool fix_min_max) :Limits<double>(min, max) { recalculate_step(fix_min_max); }

Axis::Axis(const Axis& other) :Limits<double>(other.min_, other.max_), first_(other.first_), step_(other.step_) {}

void Axis::recalculate_step(bool fix_min_max) {

    if (min_ == max_)
        min_ = max_ + 1;
    else if (min_ > max_)
        throw std::logic_error("min_ >= max_");

    wxLogMessage("min_: %g", min_);
    wxLogMessage("max_: %g", max_);

    double diff = (max_ - min_) / 10;
    wxLogMessage("(max_ - min_) / 10: %g", diff);

    Floating_point_number diff_fixed(diff);
    wxLogMessage("diff_fixed: %s", number_to_string(diff_fixed));
    double step;
    if (diff_fixed.mantissa_ > 5 && diff_fixed.mantissa_ <= 10)
        step = 10;
    else if (diff_fixed.mantissa_ > 2.5 && diff_fixed.mantissa_ <= 5)
        step = 5;
    else
        step = 2.5;
    wxLogMessage("step: %g", step);

    step_ = step * pow(10.0, diff_fixed.exponent_);

    {
        double first = min_ + step_;
        double rem = fmod(first, step_);
        if (std::abs(rem) > DBL_EPSILON)
           first -= rem;
        first_ = first;
    }

    if (fix_min_max) {
        double min = first_;
        while (min > min_)
            min -= step_ / 10;
        min_ = min;

        double max = first_;
        while (max < max_)
            max += step_ / 10;
        max_ = max;

    }

    wxLogMessage("min_: %g", min_);
    wxLogMessage("max_: %g", max_);
    wxLogMessage("first_: %g", first_);
    wxLogMessage("step_: %g", step_);

    wxLogMessage("bars count: %d", (int) ((max_ - min_) / step_));

#ifdef PLOT_DEBUG
    // todo restore, do not interate doubles
    // std::cout << min_ << " ";
    // for (double p = first_; p < max_ - DBL_EPSILON; p += step_abs)
    //      std::cout << p << " ";
    // std::cout << max_ << std::endl;
#endif 
}

Axes::Axes(Axis x, Axis y) :x_(x), y_(y) { recalculate_step(); }

void Axes::recalculate_step() {
    x_.recalculate_step();
    y_.recalculate_step();
}
