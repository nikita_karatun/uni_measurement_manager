#pragma once

#include <iostream>

struct Fixed_point_number {
    long mantissa_;
    int exponent_;
    Fixed_point_number(double);
};

void normalize(Fixed_point_number& n1, Fixed_point_number& n2);

std::ostream& operator<<(std::ostream& stream, Fixed_point_number number);

struct Floating_point_number {
    double mantissa_;
    int exponent_;
    Floating_point_number(double);
    double to_double();
};

std::ostream& operator<<(std::ostream& stream, Floating_point_number number);

template<typename T>
void min_or_default(T& original, T candidate, bool compare) {
    original = compare ? std::min(original, candidate) : candidate;
}

template<typename T>
void max_or_default(T& original, T candidate, bool compare) {
    original = compare ? std::max(original, candidate) : candidate;
}

template<typename T>
struct Limits {
    T min_, max_;
    Limits(T min, T max) :min_(min), max_(max) {}
    void add_value(const T& v, bool compare) {
        min_or_default(min_, v, compare);
        max_or_default(max_, v, compare);
    }
    T difference() const {
        return max_ - min_;
    }
};

template<typename T>
struct Limits_2 {
    Limits<T> x_;
    Limits<T> y_;
    Limits_2(Limits<T> x, Limits<T> y) :x_(x), y_(y) {}
    void add_values(const T& x, const T& y, bool compare) {
        x_.add_value(x, compare);
        y_.add_value(y, compare);
    };
    void add_values(Limits_2<T>& other, bool compare) {
        min_or_default(x_.min_, other.x_.min_, compare);
        max_or_default(x_.max_, other.x_.max_, compare);
        min_or_default(y_.min_, other.y_.min_, compare);
        max_or_default(y_.max_, other.y_.max_, compare);
    }
};

typedef Limits<double> DLimits;
typedef Limits_2<double> DLimits_2;

struct Axis : public Limits<double> {
    double first_;
    double step_;
    Axis(double min, double max, bool fix_min_max = false);
    Axis(const Axis& other);
    void recalculate_step(bool fix_min_max = false);
};

struct Axes {
    Axis x_;
    Axis y_;
    Axes(Axis x, Axis y);
    void recalculate_step();
};
