#include "stdafx.h"
#include "wx_simple_plot.h"

#include <math.h>

#include <wx/dcbuffer.h>

#include "plot_math.h"
#include "plot_painter.h"

namespace Spectrometer {

    const double Y_POINTS_OFFSET = .1;

    Plot_sizes::Plot_sizes(const SimplePlot& plot):
        plot_(plot),
        axes_(Axis(0.0, 10.0), Axis(0.0, 1.0)), 
        points_limits_(DLimits(0.0, 0.0), DLimits(0.0, 0.0)),
        x_coef_(-1.0), y_coef_(-1.0),
        window_left_offset_(60), window_right_offset_ (25), offset_(30), 
        mouse_x_(-1), mouse_y_(-1) {}

    void Plot_sizes::update() {
#ifdef PLOT_DEBUG
        wxLogMessage("y axis: min: %e, max: %e, first: %e, step: %e, bars: %f",
                axes_.y_.min_, axes_.y_.max_, axes_.y_.first_, axes_.y_.step_,
                (axes_.y_.max_ - axes_.y_.min_) / axes_.y_.step_);
#endif
        axes_.x_.recalculate_step();
        axes_.y_.recalculate_step(true);

        plot_.GetSize(&window_width_, &window_height_);

        plot_width_ = window_width_ - (window_left_offset_ + window_right_offset_);
        plot_height_ = window_height_ - offset_ * 2;

        x_coef_ = ((double) plot_width_) / (axes_.x_.max_ - axes_.x_.min_);
        y_coef_ = ((double) plot_height_) / (axes_.y_.max_ - axes_.y_.min_);

#ifdef PLOT_DEBUG
        wxLogMessage("y axis recalculated: min: %e, max: %e, first: %e, step: %e, bars: %f",
                axes_.y_.min_, axes_.y_.max_, axes_.y_.first_, axes_.y_.step_, 
                (axes_.y_.max_ - axes_.y_.min_) / axes_.y_.step_);
#endif
    }

    void Plot_sizes::update_limits(DLimits_2 limits, bool compare) {
        points_limits_.add_values(limits, compare);

        double difference = points_limits_.y_.difference();
        if (difference == 0)
            difference = std::abs(points_limits_.y_.min_);
        double gap = difference * Y_POINTS_OFFSET;
        axes_.y_.min_ = points_limits_.y_.min_ - gap;
        axes_.y_.max_ = points_limits_.y_.max_ + gap;
        update();
    }

    wxCoord Plot_sizes::x_to_pixels(double x) const {
        return (x - axes_.x_.min_) * x_coef_ + window_left_offset_;
    }

    wxCoord Plot_sizes::y_to_pixels(double y) const {
        return offset_ + plot_height_ - (y - axes_.y_.min_) * y_coef_;
    }

    Data_set::Data_set(SimplePlot* plot)
        : plot_(plot), limits_(DLimits(0.0, 0.0), DLimits(0.0, 0.0)), 
        line_pen_(wxBLACK_PEN), mark_pen_(wxBLACK_PEN), pointed_mark_pen_(wxRED_PEN)
    {}

    Data_set& Data_set::operator=(Data_set& o) {
        this->plot_ = o.plot_;
        this->points_ = o.points_;
        this->limits_ = o.limits_;
        this->line_pen_ = o.line_pen_;
        this->mark_pen_ = o.mark_pen_;
        this->pointed_mark_pen_ = o.pointed_mark_pen_;
        return *this;
    }

    void Data_set::add_point(double x, double y, bool update_limits) {
#ifdef PLOT_DEBUG
        wxLogMessage("point added: %f - %f.", x, y);
#endif
        Point point;
        point.x_ = x;
        point.y_ = y;
        point.norm_y_ = y;
        limits_.add_values(x, point.norm_y_, !points_.empty());
        points_.push_back(point);
        if (update_limits) {
            plot_->sizes_.update_limits(limits_, plot_->has_points());
        }
        plot_->points_count_++;
    }

    void Data_set::clear() {
        plot_->points_count_ -= points_.size();
        points_.clear();
    }

    Data_set& Data_set::set_line_pen(const wxPen* pen) { 
        line_pen_ = pen;
        return *this;
    }

    Data_set& Data_set::set_mark_pen(const wxPen* pen) {
        mark_pen_ = pen;
        return *this;
    }

    Data_set& Data_set::set_pointed_mark_pen(const wxPen* pen) {
        pointed_mark_pen_ = pen; 
        return *this;
    }

    SimplePlot::SimplePlot(wxWindow *parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name)
        : wxWindow(parent, id, pos, size, style, name),
        sizes_(*this), data_sets_(), plot_painter_(sizes_, data_sets_), points_count_(0)
    {
        SetBackgroundStyle(wxBG_STYLE_PAINT);
        SetBackgroundColour(wxColour("white"));
        Bind(wxEVT_PAINT, &SimplePlot::OnPaint, this);
        Bind(wxEVT_ERASE_BACKGROUND, &SimplePlot::OnEraseBackground, this);
        Bind(wxEVT_LEAVE_WINDOW, &SimplePlot::OnMouseLeave, this);
        Bind(wxEVT_MOTION, &SimplePlot::OnMouseMove, this);
        Bind(wxEVT_SIZE, &SimplePlot::OnSize, this);
    }

    Data_set& SimplePlot::create_data_set() {
        Data_set data_set(this);
        data_sets_.push_back(data_set);
        return data_sets_.back();
    }

    Data_set& SimplePlot::operator[](size_t index) {
        return data_sets_[index];
    }

    void SimplePlot::clear() {
        data_sets_.clear();
        sizes_.axes_ = Axes(Axis(0.0, 10.0), Axis(0.0, 1.0));
    }

    void SimplePlot::set_x_limits(double min, double max) {
        sizes_.axes_.x_.min_ = min;
        sizes_.axes_.x_.max_ = max;
        sizes_.axes_.x_.recalculate_step();
    }

    void SimplePlot::set_y_limits(double min, double max) {
        sizes_.axes_.y_.min_ = min;
        sizes_.axes_.y_.max_ = max;
        sizes_.axes_.y_.recalculate_step();
    }

    bool SimplePlot::has_points() {
        return points_count_ > 0;
    }

    bool SimplePlot::AcceptsFocusFromKeyboard() const { return false; }

    void SimplePlot::OnPaint(wxPaintEvent&) {
        wxAutoBufferedPaintDC dc(this);
        PrepareDC(dc);
        plot_painter_.paint(dc);
    }

    void SimplePlot::OnEraseBackground(wxEraseEvent& event) {
        // event is intersepated to prevent background from erasing
    }

    void SimplePlot::OnMouseLeave(wxMouseEvent&) {
        sizes_.mouse_x_ = -1;
    }

    void SimplePlot::OnMouseMove(wxMouseEvent& event) {
        sizes_.mouse_x_ = event.GetX();
        sizes_.mouse_y_ = event.GetY();
        Refresh(); // todo see RefreshRect()
    }

    void SimplePlot::OnSize(wxSizeEvent& event) { 
        // todo check why doesn't work
        // for (std::vector<Data_set>::iterator it = data_sets_.begin(); it != data_sets_.end(); ++it) {
        //     std::vector<Point> points = it->points_;
        //     for (std::vector<Point>::iterator pit = points.begin(); pit != points.end(); ++pit) {
        //         pit->pix_x_ = sizes_.x_to_pixels(pit->x_);
        //         pit->pix_y_ = sizes_.y_to_pixels(pit->norm_y_);
        //     }
        // }
        sizes_.update();
        Refresh();
    }

}
