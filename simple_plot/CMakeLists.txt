find_package(wxWidgets COMPONENTS core base REQUIRED)
include("${wxWidgets_USE_FILE}")

file(GLOB SOURCE_FILES ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp)

add_library(simple_plot SHARED ${SOURCE_FILES})
target_include_directories(simple_plot PRIVATE ${CMAKE_CURRENT_SOURCE_DIR}/include)
if(MSVC)
    target_link_libraries(simple_plot ${wxWidgets_LIBRARIES})
    set_target_properties(simple_plot PROPERTIES DEBUG_POSTFIX "d")
endif()

add_subdirectory(tests)
